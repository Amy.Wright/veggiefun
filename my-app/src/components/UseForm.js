import  Axios  from 'axios';
import {useState, useEffect, useCallback} from 'react';

const UseForm = (callback, validate) => {
    const [values, SetValues] = useState({
        name: '',
        email: '',
        username:'',
        password: '',
        confpassword: ''
    })
    const [errors, SetErrors] = useState({})
    const [isSubmitting, setIsSubmitting] =useState(false)

    const handleChange = e => {
        const { name, value} = e.target
        SetValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        SetErrors(validate(values));
        Axios.post('http://localhost:3001/create', (values)).then(() => console.log('success'))
        setIsSubmitting(true);
        
    }
    

    useEffect(() => {
        if(Object.keys(errors).length === 0 && isSubmitting) {
            callback()
        }
    }, [errors]);

    return {handleChange, values, handleSubmit, errors}
}

export default UseForm