import React from 'react';
import CardItem from './CardItem';
import './Cards.css';

function Cards() {
    return (
        <div className='cards'>
            <h1>Explore our site by clicking the options below!</h1>
            <div className='cards-container'>
                <div className='cards-wrap'>
                    <ul className='cards-items'>
                        <CardItem
                        src="images/forParentsCard.jpg"
                        text="For Parents"
                        desc="From healthy meals to informative articles, find great ideas to encourage your child to get 5-a-day!"

                        path='/forparents'
                        />
                        <CardItem
                        src="images/forKidsCard.jpg"
                        text="For Kids"
                        desc="Check out our activities that will keep your whole family entertained and meet our Veggie characters!"

                        path='/forkids'
                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards