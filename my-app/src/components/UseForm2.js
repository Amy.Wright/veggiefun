import {useState, useEffect, useCallback} from 'react';
import Axios from 'axios'

const UseForm2 = (callback, validate) => {
    const [values, SetValues] = useState({
        username: '',
        password: ''
    })
    const [errors, SetErrors] = useState({})
    const [isSubmitting, setIsSubmitting] =useState(false)
    const [errorMessage, setErrorMessage] =useState('')

    const handleChange = e => {
        const { name, value} = e.target
        SetValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        SetErrors(validate(values));
        Axios.post('http://localhost:3001/login', (values)).then((response) => console.log(response))
        setIsSubmitting(true);
        if(isSubmitting === true){
            //Signin Success
            localStorage.setItem("isAuthenticated", "true");
            window.location.pathname = "/userprofile";
        }
    }

    useEffect(() => {
        if(Object.keys(errors).length === 0 && isSubmitting) {
            callback()
        }
    }, [errors]);

    return {handleChange, values, handleSubmit, errors}
}

export default UseForm2