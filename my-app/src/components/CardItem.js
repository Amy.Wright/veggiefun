import React from 'react';
import { Link } from 'react-router-dom';

function CardItem(props) {
    return (
        <>
            <li className='cards-items'>
                <Link to={props.path} className='cards-items-link'>
                    <figure className='cards-items-pic' data-catagory={props.label}>
                        <img src={props.src} alt="Image" className='cards-items-img'>

                        </img>
                    </figure>
                    <div className='cards-items-desc'>
                        <h5 className='cards-items-txt'>{props.text}</h5>
                        <p className='secondtext'>{props.desc}</p>
                    </div>
                </Link>
            </li>
        </>
    )
}

export default CardItem