import React from 'react';
import '../App.css'
import Button from './Button';
import './TabSection.css';
import { Link } from 'react-router-dom';
import background from './pages/images/home-tabsection.jpg';

function TabSection(){
    return (
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
<div className='trans-box'>
            <h1>Welcome to VeggieFun!</h1>
            <p>Explore our site to find fun ways to encourage your child to get their 5-a-day!</p>
            </div>
        </div>
    )
}

export default TabSection