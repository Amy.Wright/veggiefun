export default function ValidateForm(values) {
    let errors = {}
    
    if(!values.name.trim()) {
        errors.name = "Name required"
    }

    if(!values.username) {
        errors.username = "Username required"
    }

    if(!values.email) {
        errors.email = "Email required"
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = 'Email address is invalid';
    }

    if(!values.password) {
        errors.password = "Password is required"
    } else if (values.password.length < 8) {
        errors.password = 'Password needs to be 8 characters or more';
    }

    if (!values.confpassword) {
        errors.confpassword = 'Password is required';
      } else if (values.confpassword !== values.password) {
        errors.confpassword = 'Passwords do not match';
      }
      return errors;
}