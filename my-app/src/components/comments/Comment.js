import React from 'react';
import { deleteComment, updateComment } from './api';
import CommentForm from './CommentForm';

const Comment = ({comment, replies, currentUserId, deleteComment, activeComment, addComment, setActiveComment, updateComment, parentId = null}) => {
    const canReply = Boolean(currentUserId)
    const canEdit = currentUserId === comment.userId
    const canDelete = currentUserId === comment.userId
    const createdAt = new Date(comment.createdAt).toLocaleDateString();
    const isReplying = activeComment && activeComment.type === 'replying' && activeComment.id === comment.id;
    const isEditing = activeComment && activeComment.type === 'editing' && activeComment.id === comment.id;
    const replyId = parentId ? parentId : comment.id;
    return (
        <div className='comment'>
            <div className='commenter-icon'>
                <img src="/images/heart outline.png" width='30' height='30'/>
            </div>
            <div className='comment-right'>
                <div className='contentOfComment'>
                    <div className='commentor'>{comment.username}</div>
                    <div>{createdAt}</div>
                </div>
                {!isEditing && <div className='comment-text'>
                    {comment.body}
                </div>} {isEditing && (
                    <CommentForm submitLabel="Update" hasCancelButton initialText={comment.body} handleSubmit={(text) => updateComment(text, comment.id)} handleCancel={() => setActiveComment(null)}/>
                )}
                <div className='comment-actions'>
                    {canReply && (<div className='comment-action' onClick={() => setActiveComment({id: comment.id, type: "replying"})}>Reply</div>)}
                    {canEdit &&<div className='comment-action' onClick={() => setActiveComment({id:comment.id, type: "editing"})}>Edit</div>}
                    {canDelete && (<div className='comment-action' 
                    onClick={() => deleteComment(comment.Id)}>
                        Delete
                </div>)}
                </div>
                {isReplying && (
                    <CommentForm submitLabel="Reply" handleSubmit={(text) => addComment(text, replyId)}/>
                )}
                {replies.length > 0 && (
                    <div className='replies'>
                        {replies.map(reply =>(
                            <Comment comment={reply} key={reply.id} replies={[]} currentUserId={currentUserId} deleteComment={deleteComment} addComment={addComment} updateComment={updateComment} activeComment={activeComment} setActiveComment={setActiveComment} parentId={comment.id}/>
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}

export default Comment