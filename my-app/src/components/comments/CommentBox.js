import React, { useEffect, useState } from 'react';
import Button from './../Button'
import {getComments as getCommentsApi, createComment as createCommentApi, deleteComment as deleteCommentApi, updateComment as updateCommentApi} from './api'
import Comment from './Comment'
import CommentForm from './CommentForm';
import './comment.css'

const CommentBox = ({currentUserId}) => {
    const [backendComments, setBackendComments] = useState([])
    const [activeComment, setActiveComment] = useState(null)
    const rootComments = backendComments.filter(
        (backendComment) => backendComment.parentId === null);
    const getReplies = (commentId) => 
        backendComments.filter((backendComment) => backendComment.parentId === commentId).sort(
            (a,b) => 
            new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
    const addComment = (text, parentId) => {
        console.log("addComment", text, parentId);
        createCommentApi(text, parentId).then(comment => {
            setBackendComments([comment, ...backendComments]);
            setActiveComment(null);
        })
    };
    const deleteComment = (commentId)=> {
        if(window.confirm('Do you want to delete this comment?')){
            deleteCommentApi().then(() => {
                const updatedBackendComments = backendComments.filter(
                    (backendComment) => backendComment.id !== commentId);
                setBackendComments(updatedBackendComments);
            })
        }
    }
    const updateComment = (text, commentId) => {
        updateCommentApi(text, commentId).then(() => {
            const updatedbackendComments = backendComments.map(backendComment => {
                if (backendComment.id === commentId) {
                    return {...backendComment, body: text}
                }return backendComment
            })
            setBackendComments(updatedbackendComments)
            setActiveComment(null)
        })
    }
        useEffect(() => {
        getCommentsApi().then((data) => { 
            setBackendComments(data);
        });
    }, []);
    return (
        <div className='comments'>
            <h3 className='comments-heading'>Comments</h3>
            <div className='comment-title'>Leave a comment</div>
            <CommentForm submitLabel="Comment" handleSubmit={addComment}/>
            <div className='comments-container'>
                {rootComments.map(rootComment => (
                  <Comment key={rootComment.id} comment={rootComment} replies={getReplies(rootComment.id)}
                  currentUserId={currentUserId}
                  deleteComment={deleteComment}
                  activeComment={activeComment}
                  setActiveComment={setActiveComment}
                  updateComment={updateComment}
                  addComment={addComment}/>
                ))}
            </div>
        </div>
    )
}

export default CommentBox


