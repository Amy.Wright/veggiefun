import React from 'react';
import {useState} from 'react';

const CommentForm = ({handleSubmit, submitLabel, hasCancelButton=false, initialText='', handleCancel}) => {
    const [text, setText] = useState(initialText);
    const isTextAvailable = text.length === 0;
    const onSubmit = (event) => {
        event.preventDefault();
        handleSubmit(text);
        setText("");
    }
    return(
        <form onSubmit={onSubmit}>
            <textarea 
            className='comment-textarea' 
            value={text} 
            onChange={(e) => setText(e.target.value)}/>
            <button className='comment-button' disabled={isTextAvailable}>{submitLabel}</button>
            {hasCancelButton && (<button type='button' className='comment-button-cancel' onClick={handleCancel}>Cancel</button>)}
        </form>
    )
};

export default CommentForm