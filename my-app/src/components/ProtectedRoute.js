import React, { Children } from "react";
import { Navigate, Route } from "react-router-dom";

export const ProtectedRoute = ({ children}) => {
    const isAuthenticated = true;
        
    if (isAuthenticated ) {
      return children
    }
      
    return <Navigate to="/login" />
  }

export default ProtectedRoute;