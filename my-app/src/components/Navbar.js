import React, {useState} from 'react';
import { Link, NavLink } from 'react-router-dom';
import Button from './Button';
import './Navbar.css';
import { useEffect } from 'react';
import logo from './pages/images/logo.png'

/* This code sets up a responsive global navbar
*
*/
function Navbar() {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
    
    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false)
        } else {
            setButton(true)
        }
    };

    useEffect(() => {
        showButton();
    }, []);

    window.addEventListener('resize', showButton)
    return (
        <>
            <nav className="navar">
               <div className="navbar-container">
                   <img src={logo}/>
                   <Link to ="/" className="navbar-logo" onClick={closeMobileMenu}>
                       VeggieFun <i className="fab fa-typo3" />                  
                   </Link>
                   <div className="menu-icon" onClick={handleClick}>
                        <i className={click ? "fas fa-times" : "fas fa-bars"} />
                   </div>
                   <ul className={click ? "nav-menu active" : "nav-menu"}>
                       <li className="nav-item">
                           <NavLink to="/" className="nav-links" onClick={closeMobileMenu} activeClassName='active'>
                               Home
                           </NavLink>
                       </li>
                       <li className="nav-item">
                           <NavLink to="/forparents" className="nav-links" onClick={closeMobileMenu}>
                               For Parents
                           </NavLink>
                       </li>
                       <li className="nav-item">
                           <NavLink to="/forkids" className="nav-links" onClick={closeMobileMenu}>
                               For Kids
                           </NavLink>
                       </li>
                       <li className="nav-item">
                           <Link to="/signup" className="nav-links-mobile" onClick={closeMobileMenu}>
                               Sign Up/Login
                           </Link>
                       </li>
                   </ul>
                   {button && <Link to='/signup'><Button buttonStyle='btn-outline'>Sign Up/Login</Button></Link>}
               </div>
            </nav>
        </>
    )
}

export default Navbar