import React, {Component} from 'react'
import {useState} from 'react';
import bronze from './BronxeCertificate.pdf'
import silver from './SilverCertificate.pdf'
import gold from './GoldCertificate.pdf'
import Button from './../Button';
import './Certificates.css'

const printIframe = (id) => {
    const iframe = document.frames
      ? document.frames[id]
      : document.getElementById(id);
    const iframeWindow = iframe.contentWindow || iframe;
  
    iframe.focus();
    iframeWindow.print();
  
    return false;
  };

const Certificates= () => {
    return (
        <div>
            <div className='cert'>
            <iframe id='bronze' src={bronze} height="205" ></iframe>
            <Button onClick={() => printIframe('bronze')}>Print Bronze</Button>
            </div>
            <div className='cert'>
            <iframe id='silver' src={silver} height="205" ></iframe>
            <Button onClick={() => printIframe('silver')}>Print Silver</Button>
            </div>
            <div className='cert'>
            <iframe id='gold' src={gold} height="205" ></iframe>
            <Button onClick={() => printIframe('gold')}>Print Gold</Button>
            </div>
        </div>
    )

}
export default Certificates