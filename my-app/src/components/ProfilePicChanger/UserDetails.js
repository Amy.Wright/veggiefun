import React, {Component} from 'react'
import './UserDetails.css'
import {useState} from 'react';


const UserDetails= ({initialText=''}) => {
    const [text, setText] = useState(initialText);
    return (
        <div>
         <form>
             <div className='inputs'>
         <label className='user-label'>
                Name:
         </label> 
         <input
                    id='userName'
                    type='text' 
                    name='userName' 
                    placeholder='Enter your name'
                    />
                    </div>
                    <div className='inputs'>
                     <label className='user-label'>
                Age:
         </label> 
         <input
                    id='userAge'
                    type='text' 
                    name='userAge' 
                    placeholder='Enter your age'
                    />
                    </div>
                    <div className='inputs'>
                     <label className='user-label'>
                Favourite Vegetable:
         </label> 
         <input
                    id='userVeg'
                    type='text' 
                    name='userVeg' 
                    placeholder='Enter your favorite vegetable'
                    />
                    </div>
        <label className='user-label'>Points:</label>
        </form>
        </div>
    )

}
export default UserDetails