import React, {Component} from 'react'
import background from './../pages/images/forKidsBanner.png';
import Button from './../Button';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';

import { Modal} from 'antd';

class ProfilePicChanger extends Component {
    constructor(props){
        super(props)
        this.state={
            visible: false,
            profileImages: [props.pic1, props.pic2, props.pic3, props.pic4, props.pic5, props.pic6]
        }
    }

    showModal = () => {
        this.setState({
            visible: true,
        })
    }

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        })
    }

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false
        })
    }
    render(){
        const imageMapper = this.state.profileImages.map((image, index) => {
            return (
                <img src={image} onClick={() => this.props.handleImageChange(image)} height="100px" width="100px"/>
        )
        })
    return (
        <div>
         <Button type="primary" onClick={this.showModal}>
        Change Avatar
      </Button>
      <Modal title="Change Avatar" visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}>
        {imageMapper}
      </Modal>
        </div>
    )
}
}
export default ProfilePicChanger