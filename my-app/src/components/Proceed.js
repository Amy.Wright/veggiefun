import React from 'react'
import './Form.css';

import { Link } from 'react-router-dom';

const Proceed = () => {
    return (
        <div className='form-content-input'>
            <div className='signupsuccess'>Congratulations! Please click the button below to take you to your user profile.</div>
            {/*<img src="img address" alt="successimg" className="form-img-success"/>*/}
            <Link to='/userprofile'><div className='signup-form'><button className='form-input-btn'>User Profile</button></div></Link>
        </div>
    )
}

export default Proceed