import React from 'react';
import UseForm2 from '../UseForm2';
import validate from '../ValidateForm2';
import '../Form.css';

const LoginForm = ({submitForm}) => {
    const {handleChange, values, handleSubmit, errors} = UseForm2(submitForm, validate);
    return (
        <div className='form-content-input'>
        <form className='signup-form' onSubmit={handleSubmit}>
            <h1>Join our growing community by creating your account below! </h1>
            <div className='form-inputs'>
                <label htmlFor='firstname' className='form-label'>
                Username:
                </label>  
                    <input
                    id='username'
                    type='text' 
                    name='username' 
                    className='form-input'
                    placeholder='Enter your username'
                    value={values.username}
                    onChange={handleChange}
                    />
                    {errors.username && <p>{errors.username}</p>}
            </div>
            <div className='form-inputs'>
                <label htmlFor='password' className='form-label'>
                Password:
                </label>
                    <input 
                    id='password'
                    type='text' 
                    name='password' 
                    className='form-input'
                    placeholder='Enter your password'
                    value={values.password}
                    onChange={handleChange}
                    /> 
                    {errors.password && <p>{errors.password}</p>} 
            </div>
            <button onClick={handleSubmit} className='form-input-btn'  type='submit'>Login</button>
            <span className='form-input-login'>
                New? Create an account <a href='/signup'>here</a>
            </span>
        </form>
        </div>
    )
}

export default LoginForm