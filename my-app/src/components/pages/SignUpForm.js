import React, { useState } from 'react';
import UseForm from '../UseForm';
import validate from '../ValidateForm';
import '../Form.css';
import Axios from 'axios'

const SignUpForm = ({submitForm}) => {
    const {handleChange, values, handleSubmit, errors} = UseForm(submitForm, validate);
    
    return (
        
        <div className='form-content-input'>
            <form className='signup-form' onSubmit={handleSubmit}>
                <h1>Join our growing community by creating your account below! </h1>
                <div className='form-inputs'>
                    <label htmlFor='name' className='form-label'>
                    Name:
                    </label>  
                        <input
                        id='name'
                        type='text' 
                        name='name' 
                        className='form-input'
                        placeholder='Enter your name'
                        value={values.name}
                        onChange={handleChange}
                        />
                        {errors.name && <p>{errors.name}</p>}
                </div>
                <div className='form-inputs'>
                    <label htmlFor='email' className='form-label'>
                    Email:
                    </label>
                        <input 
                        id='email'
                        type='email' 
                        name='email' 
                        className='form-input'
                        placeholder='Enter your email'
                        value={values.email}
                        onChange={handleChange}
                        />
                        {errors.email && <p>{errors.email}</p>}
                </div>
                <div className='form-inputs'>
                    <label htmlFor='username' className='form-label'>
                    Username:
                    </label>
                        <input 
                        id='username'
                        type='username' 
                        name='username' 
                        className='form-input'
                        placeholder='Enter your username'
                        value={values.username}
                        onChange={handleChange}
                        />
                        {errors.username && <p>{errors.username}</p>}
                </div>
                <div className='form-inputs'>
                    <label htmlFor='password' className='form-label'>
                    Password:
                    </label>
                        <input 
                        id='password'
                        type='password' 
                        name='password' 
                        className='form-input'
                        placeholder='Enter your password'
                        value={values.password}
                        onChange={handleChange}
                        />
                        {errors.password && <p>{errors.password}</p>}
                </div>
                <div className='form-inputs'>
                    <label htmlFor='confpassword' className='form-label'>
                    Confirm Password:
                    </label>
                        <input 
                        id='confpassword'
                        type='password' 
                        name='confpassword' 
                        className='form-input'
                        placeholder='Confirm your password'
                        value={values.confpassword}
                        onChange={handleChange}
                        />
                        {errors.confpassword && <p>{errors.confpassword}</p>}
                </div>
                <button  onClick={handleSubmit} className='form-input-btn'  type='submit'>Sign Up</button>
                <span className='form-input-login'>
                    Already have an account? Login <a href='/login'>here</a>
                </span>
            </form>
        </div>
    )
}


export default SignUpForm