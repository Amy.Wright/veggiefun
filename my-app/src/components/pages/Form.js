import React, {useState} from 'react';
import SignUpForm from './SignUpForm';
import '../Form.css'
import UserProfile from './UserProfile';
import Proceed from '../Proceed';

const Form = () => {
    const [isSubmitted, setIsSubmitted] = useState(false)

    function submitForm() {
        setIsSubmitted(true);
    }
    return (
        <>
        <div className='form-container'>
            <div className='form-content-left'>
                {/*<img src="" alt="" className="form-img />*/}
            </div>
            {!isSubmitted ? (<SignUpForm submitForm={submitForm} />) : <Proceed/>}
        </div>
        
           
        
        </>
    )
}

export default Form