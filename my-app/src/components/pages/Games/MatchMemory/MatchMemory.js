import React, { Suspense, useEffect, useState } from 'react';
import './MatchMemory.css';
import im1 from './../../images/onioncardgame.jpg';
import im2 from './../../images/carrotcardgame.jpg';
import im3 from './../../images/broccolicardgame.jpg';
import im4 from './../../images/tomatocardgame.jpg';
import im6 from './../../images/peascardgame.jpg';
import im5 from './../../images/peppercardgame.jpg';
import back from './../../images/cardBck.jpg';
import SingleCard from './SingleCard';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import background from './../../images/forKidsBanner.png';

const cardImgs = [
    { "src" : im1, matched:false},
    { "src" : im2, matched:false},
    { "src" : im3, matched:false},
    { "src" : im4, matched:false},
    { "src" : im6, matched:false},
    { "src" : im5, matched:false}
]

function MatchMemory() {
    const [cards, setCards] = useState([])
    const [turns, setTurns] = useState(0)
    const [cardOne, setCardOne] = useState(null)
    const [cardTwo, setCardTwo] = useState(null)
    const [disabled, setDisabled] = useState(false)
    const [score, setScore] = useState(0)

    const shuffle = () => {
        const shuffleCard = [...cardImgs, ...cardImgs]
        .sort(() => Math.random() - 0.5)
        .map((card) => ({...card, id: Math.random()}))
        setCardOne(null)
        setCardTwo(null)
        setCards(shuffleCard)
        setTurns(0)
        setScore(0)
    }
    
    const handleChoice = (card) => {
        cardOne ? setCardTwo(card) : setCardOne(card)
    }

    useEffect(() => {
        if (cardOne && cardTwo){
        setDisabled(true)
            if(cardOne.src === cardTwo.src){
                setCards(prevCards => {
                    return prevCards.map(card => {
                        if(card.src === cardOne.src){
                            return (score + 1, {...card, matched: true} )
                        }else{
                            return card
                        }
                    })
                   
                })
                resetTurn()
            } else {
                setTimeout(() => resetTurn(), 500)
            }
        }
    }, [cardOne, cardTwo])

    const resetTurn = () => {
        setCardOne(null)
        setCardTwo(null)
        setTurns(prevTurns => prevTurns + 1) 
        setDisabled(false)
    }


    useEffect(() => {
        shuffle()
    }, [])

    return(
        <div>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Card Match</h1>
            <p>Try and remember where the pairs of cards are.</p></div> 
            <div className='tab-btns'>
                    <Link to='/forKids'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Kids 
                </Button>
                </Link>   
            </div>
        </div>
            <div className='gameContainer'>
            <h1>Instructions</h1>
            <p className='step'>Click a card to turn it over. Then click another card and try to match the image to the first card.</p>
            <button className='buttonA' onClick={shuffle}>New Game</button>
            <p className='info'>Turns: {turns}</p>
            <p className='info'>Score: {score}</p>
            <div className='gameGrid'>
                {cards.map(card => (
                    <SingleCard key={card.id} card={card} back={back} handleChoice={handleChoice} flipped={card === cardOne || card === cardTwo || card.matched} disabled={disabled}/>
                ))}
            </div>
            </div>   
        </div>

    )
} 

export default MatchMemory