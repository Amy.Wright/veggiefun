import './SingleCard.css'

export default function SingleCard({card, back, handleChoice, flipped, disabled}) {
    const handleClick = () => {
        if(!disabled) {
            handleChoice(card)
        }
        
    }
    
    return (
        <div className='container'>
            <div className='card'>
                <div className={flipped ? 'flipped' : ''}>
                    <img className='flip' src={card.src} alt='flip' />
                    <img className='back' src={back} alt='back' onClick={handleClick}/>
                </div>
            </div>
        </div>
    )
}