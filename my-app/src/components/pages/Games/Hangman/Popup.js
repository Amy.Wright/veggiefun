import React, { useEffect} from 'react';
import { CheckWin } from './Checkers';
import './Hangman.css'

const Popup = ({correctGuesses, incorrectGuesses, selectedWord, setPlayable, playAgain}) => {
  let endMessage ='';
  let endMessageRevealWord = '';
  let playable = true;

  if(CheckWin(correctGuesses, incorrectGuesses, selectedWord) === 'win'){
    endMessage= "Congratulations! You guessed the vegetable.";
    playable = false;
  }else if (CheckWin(correctGuesses, incorrectGuesses, selectedWord) === 'lose'){
    endMessage= "Oh no! You ran out of guesses.";
    endMessageRevealWord = `The word was: ${selectedWord}`;
    playable = false;
  }

  useEffect(() => setPlayable(playable));

  return (
  <div className="popup-container" style={endMessage !== '' ? {display:'flex'} : {}}>
      <div className="popup">
        <h2>{endMessage}</h2>
        <h3>{endMessageRevealWord}</h3>
        <button onClick={playAgain}>Play Again</button>
      </div>
    </div>
        )
    }
    
    export default Popup