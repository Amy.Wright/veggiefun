import React from 'react';

const IncorrectGuesses = ({incorrectGuesses}) => {
  return (
    <div className="incorrect-guesses-container">
    <div>
    {incorrectGuesses.length > 0 && <p>Wrong Guesses</p>}
    {incorrectGuesses
    .map((letter, i) => <span key={i}>{letter}</span>)
    .reduce((prev, curr) => prev === null ? [curr] : [prev, ', ', curr], null)}
    </div>
  </div>
    )
}

export default IncorrectGuesses