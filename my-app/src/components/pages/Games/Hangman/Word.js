import React from 'react';
import './Hangman.css'

const Word = ({ selectedWord, correctGuesses }) => {
  return (
    <div className="word">
       {selectedWord.split('').map((letter, i) => {
         return(
          <span className="letter" key={i}>
            {correctGuesses.includes(letter) ? letter : ''}
          </span>
         )})}
    </div>
    )
}

export default Word