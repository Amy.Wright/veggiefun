import React, { useState, useEffect } from 'react';
import './Hangman.css'
import Frame from './Frame.js'
import '../../../../App.css';
import IncorrectGuesses from './IncorectGuesses';
import Word from './Word';
import Popup from './Popup';
import Notification from './Notification'
import { ShowNotification as show} from './Checkers';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import background from './../../images/forKidsBanner.png';


const words = ['cucumber', 'broccoli', 'onion', 'carrot', 'tomato'];

let selectedWord = words[Math.floor(Math.random() * words.length)];


export default function Hangman(){
    const [playable, setPlayable] = useState(true);
    const [correctGuesses, setCorrectGuesses] = useState([]);
    const [incorrectGuesses, setIncorrectGuesses] = useState([]);
    const [showNotification, setShowNotification] = useState(false);

    useEffect(() => {
        const handleKeyPress = event => {
            const { key, keyCode} = event;
                if (playable && keyCode >= 65 && keyCode <= 90) {
                    const letter = key.toLowerCase();
        
                    if (selectedWord.includes(letter)) {
                        if (!correctGuesses.includes(letter)) {
                            setCorrectGuesses(currentLetters => [...currentLetters, letter]);
                        } else {
                            show(setShowNotification);
                        }
                    } else {
                        if (!incorrectGuesses.includes(letter)) {
                            setIncorrectGuesses(incorrectGuesses => [...incorrectGuesses, letter]);
                        } else {
                            show(setShowNotification);
                        }
                    }
                }
            }
            window.addEventListener('keydown', handleKeyPress)       
            return () => window.removeEventListener('keydown', handleKeyPress)
         }, [correctGuesses, incorrectGuesses, playable]);
    
         
         function playAgain() {
             setPlayable(true);
             setCorrectGuesses([]);
             setIncorrectGuesses([]);

             const random =  Math.floor(Math.random() * words.length);
             selectedWord = words[random];
         }
         
    return(
        <>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Hangman</h1>
            <p>Try to guess the word before the picture is complete!.</p></div>
            <div className='tab-btns'>
                    <Link to='/forKids'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Kids  
                </Button>
                </Link>   
            </div>
        </div>
        <div className='gameContainer'>
        <h1>Instructions</h1>
        <p className='instructions'>Enter a letter using your keyboard to see if it appears in the word. The word will be a vegetable, count how many letters the word is to get a hint.</p>
        <div className='hangman-container'>
            <Frame incorrectGuesses={incorrectGuesses}/>
            <IncorrectGuesses incorrectGuesses={incorrectGuesses}/>
            <Word selectedWord={selectedWord} correctGuesses={correctGuesses}/>
        </div>
        <Popup correctGuesses={correctGuesses} incorrectGuesses={incorrectGuesses} selectedWord={selectedWord} setPlayable={setPlayable} playAgain={playAgain}/>
         <div> <Notification showNotification={showNotification}/> </div>  
         </div>
        </>
    )
}