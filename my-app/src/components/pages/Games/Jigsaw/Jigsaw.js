import React, {useState, useEffect} from 'react'
import Board from './Board'
import './jigsaw.css'
import {updateURLParameter} from './helpers'
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import background from './../../images/forKidsBanner.png';

function Jigsaw() {
    const [imgUrl, setImgUrl] = useState("")
    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search)
        if(urlParams.has("img")) {
            setImgUrl(urlParams.get("img"))
        }
    }, [])
    const handleImageChange = (e) => {
        setImgUrl(e.target.value)
        window.history.replaceState("","",updateURLParameter(window.location.href, "img", e.target.value ))
    }
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Jigsaw Puzzle</h1>
        <p>Try and rearrange the tiles to form the correct image.</p></div>
        <div className='tab-btns'>
                <Link to='/forKids'>
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>
                Back to Kids 
            </Button>
            </Link>   
        </div>
    </div>
    <div className='gameContainer'>
        <div className='jigsaw'>
            <h1> Instructions</h1>
            <p> Click 'Start Game' to shuffle the tiles. Click the tiles to try and form the correct image of Brad the Broccoli!</p>
            <h2> Did you know?</h2>
            <p>Tom “Broccoli” Landers holds the current world record for eating 1 pound of broccoli in 92 seconds.</p>
            <p>
            The United States is the 3rd largest broccoli producer in the world (after China and India).</p>
        <p>
Heaviest broccoli was grown by John and Mary Evans of Palmer, Alaska, USA in 1993 weighed 15.87 kg (35 lb).
            </p>
            <Board imgUrl={imgUrl}/>
            <p>Broccoli image https://i.pinimg.com/originals/9c/59/0f/9c590f7b674994dadcc11c06885e2502.png</p>
            <input value={imgUrl} onChange={handleImageChange}/>
        </div>
        </div>
        </div>
    )
}

export default Jigsaw