import React from 'react';
import '../../../App.css';
import CardItem from './../../CardItem';
import './../../Cards.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import background from './../images/pens.jpg';

export default function CrossWords(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'> <h1>Crosswords</h1>
            <p>Test your knowledge with these fun word puzzles</p></div>
            <div className='tab-btns'>
            <Link to='/activities'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Activities  
                </Button>
                </Link>
            </div>
        </div>
        <div className='cards'>            
            <h1>Crosswords</h1>
            <div className='cards-container'>
                <div className='cards-wrap'>
                <div>
                    <ul className='cards-items'>
                        <CardItem
                        src="images/vegetables.jpg"
                        text="Veggies"
                        label="20 mins"
                        path='/vegetablecrossword' />
                    </ul>
                    </div>
                    </div>
                </div>
        </div>
        </div>
        )
        }