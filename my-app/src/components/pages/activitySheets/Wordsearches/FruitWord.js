import React from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import '../../../../App.css';
import background from './../../images/fruit.jpg';
import popout from './../../images/popout.jpg';
import './../activity.css'


export default function FruitWord(){
    return (
        <div className='pdfpage'>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <h1>Fruit Wordsearch</h1>
            <div className='tab-btns'>
            <Link to='/wordsearches'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Wordsearches  
                </Button>
                </Link>  
            </div>
            </div>
            <p>
            Print this sheet by selecting the button below or by selecting the <img src={popout} width={30} height={30}/> icon to open it on your personal device.
            </p>
            <div className='activity-pdf'><iframe src="https://drive.google.com/file/d/1AGBs7QS-SgLVz2gQyJ1gowRsLDaGHjbw/preview" width="640" height="750" allow="autoplay"></iframe></div>
            <div  className="print-btns">
            <Button onClick={() => window.print()}>Print</Button>
            </div>
        </div>

    )
    }
