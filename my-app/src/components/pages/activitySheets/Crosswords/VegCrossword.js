import React from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import '../../../../App.css';
import './../activity.css'
import background from './../../images/vegetables.jpg';
import popout from './../../images/popout.jpg'
import crossword from './Vegetable_Crossword.pdf'

const printIframe = (id) => {
    const iframe = document.frames
      ? document.frames[id]
      : document.getElementById(id);
    const iframeWindow = iframe.contentWindow || iframe;
  
    iframe.focus();
    iframeWindow.print();
  
    return false;
  };

export default function VegCrossword(){
    return (
        <div className='pdfpage'>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'>  <h1>Vegetables Crossword</h1> </div>
            <div className='tab-btns'>                
            <Link to='/crosswords'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Crosswords  
                </Button>
                </Link>
            </div>   
            </div>
            <p>
            Print this sheet by selecting the button below or by selecting the <img src={popout} width={30} height={30}/> icon to open it on your personal device.
            </p>
            <div className='activity-pdf'><iframe src={crossword} width="640" height="830" allow="autoplay" id='crossword'></iframe></div>
            <div  className="print-btns">
            <Button onClick={() => printIframe('crossword')}>Print</Button>
            </div>
        </div>

    )
    }