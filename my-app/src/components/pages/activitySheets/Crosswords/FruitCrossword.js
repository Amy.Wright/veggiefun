import React from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import '../../../../App.css';
import './../activity.css'
import background from './../../images/fruit.jpg';
import popout from './../../images/popout.jpg'

export default function FruitCrossword(){
    return (
        <div className='pdfpage'>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <h1>Fruit Crossword</h1>
            <div className='tab-btns'>
            <Link to='/crosswords'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Crosswords  
                </Button>
                </Link>
            </div>
            </div>
            <p>
            Print this sheet by selecting the button below or by selecting the <img src={popout} width={30} height={30}/> icon to open it on your personal device.
            </p>
            <div className='activity-pdf'><iframe src="https://drive.google.com/file/d/1JYqRUTwLcILCJF-pFhQ_VlJxfJ1ukknt/preview" width="640" height="830" allow="autoplay"></iframe></div>
            <div  className="print-btns">
            <Button onClick={() => window.print()}>Print</Button>
            </div>
        </div>

    )
    }