import React from 'react';
import '../../../App.css';
import CardItem from './../../CardItem';
import './../../Cards.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import background from './../images/wordsearches.jpg';

export default function Wordsearches(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Wordsearches</h1>
            <p>Try and find all the hidden words inside the wordsearch!</p></div>
            <div className='tab-btns'>
            <Link to='/activities'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Activities  
                </Button>
                </Link>  
            </div>
        </div>
        <div className='cards'>            
            <h1>Wordsearches</h1>
            <div className='cards-container'>
                <div className='cards-wrap'>
                <div>
                    <ul className='cards-items'>
                        <CardItem
                        src="images/vegetables.jpg"
                        text="Veggies"
                        label="5 mins"
                        path='/vegword' />
                    </ul>
                    </div>
                    </div>
                </div>
        </div>
        </div>
        )
        }