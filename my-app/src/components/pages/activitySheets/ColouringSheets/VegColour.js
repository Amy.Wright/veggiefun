import React from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import '../../../../App.css';
import './../activity.css'
import background from './../../images/vegetables.jpg';
import popout from './../../images/popout.jpg'
import colour from './Veg-Colour.pdf'



export default function VegColour(){
    const printIframe = (id) => {
        const iframe = document.frames
          ? document.frames[id]
          : document.getElementById(id);
        const iframeWindow = iframe.contentWindow || iframe;
      
        iframe.focus();
        iframeWindow.print();
      
        return false;
      };
    return (
        <div className='pdfpage'>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Vegetables Colouring Sheet</h1></div>
            <div className='tab-btns'>
            <Link to='/colouringsheets'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Colouring Sheets  
                </Button>
                </Link>
            </div>
            </div>
            <p>
            Print this sheet by selecting the button below or by selecting the <img src={popout} width={30} height={30}/> icon to open it on your personal device.
            </p>
        <div className='activity-pdf'><iframe src={colour} width="640" height="900" allow="autoplay" id='colour'></iframe></div>
        <div  className="print-btns">
            <Button onClick={() => printIframe('colour')}>Print</Button>
            </div>
        </div>

    )
    }