import React from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import '../../../../App.css';
import './../activity.css'
import background from './../../images/fruit.jpg';
import popout from './../../images/popout.jpg'

export default function FruitColour(){
    return (
        <div className='pdfpage'>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <h1>Fruit Colouring Sheet</h1>
            <div className='tab-btns'>
            <Link to='/colouringsheets'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Colouring Sheets  
                </Button>
                </Link>
            </div>
            </div>
        <p>
            Print this sheet by selecting the button below or by selecting the <img src={popout} width={30} height={30}/> icon to open it on your personal device.
            </p>
            <div className='activity-pdf'><iframe src="https://drive.google.com/file/d/1I_x7upEuoS7gF48jxZ6lz6aaiRRgGR-m/preview" width="640" height="900" allow="autoplay"></iframe></div>
            <div  className="print-btns">
            <Button onClick={() => window.print()}>Print</Button>
            </div>
        </div>

    )
    }

 