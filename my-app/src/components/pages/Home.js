import React from 'react';
import '../../App.css';
import TabSection from '../TabSection.js';
import Cards from '../Cards';

function Home () {
    return (
        <>
            <TabSection />
            <Cards />
        </>
    )
}
export default Home;