import React from 'react';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import '../../../App.css';
import health from './../images/healtharticle.jpg';
import background from './../images/home-tabsection.jpg';

export default function HealthBenefits(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Health Benefits</h1>
        <p>Fruits and vegetables are very important to a child's development. Find out more below.</p></div>
        <div className='tab-btns'>
        <Link to='/forparents'>    
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>
                Back to Parents
            </Button>
            </Link>   
        </div>
    </div>
    <div className='inform-body'>
    <p className='idea-content'>Vegetables are very important to your childs development and fostering a good relationship between them from an early age can reduce the risks of many health complications later in life.
    A vegetable rich diet can lower blood pressure, reduce the risk of heart disease and stroke, prevent some types of cancer, lower risk of eye and digestive problems and have a positive effect on blood pressure.
    However, no one vegetavle can do this - variety is key. It is recommended children aged 1-3 years should have 2-3 servings of vegetables per day and children aged 4-8 should have 4 and a half.</p>
    <div className='tab-container2' style={{backgroundImage: `url(${health})`, alignSelf:'center' }}></div>
    <h3 className='idea-title'>Set a Good Example</h3>
    <p className='idea-content'>Children learn from watching their role models so it is important for you to consume your veggies too! If your child doesn't like a vegetable at first, that is normal.
    Research shows a child can try a new food ten times before they start to accept it and liking it can sometimes take even longer. However, it is importaant to praise your child for trying as this will positive reaction will increase thier likelhood for trying again.</p>
    </div>
    </div>

    )
    }