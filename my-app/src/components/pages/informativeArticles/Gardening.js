import React from 'react';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import '../../../App.css';
import background from './../images/home-tabsection.jpg';
import quickVeg from './../images/quickVeg.jpg';
import compost from './../images/compost.jpg';
import cupboard from './../images/cupboard.jpg';
import plantpot from './../images/plantpot.jpg';
import scrapbook from './../images/scrapbook.jpg';
import './informative.css'

export default function Gardening(){
    return (
        <div>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Gardening</h1>
            <p>Gardening can be a great way to involve the whole family and cut down that grocery bill!</p></div>
            <div className='tab-btns'>
                    <Link to='/forparents'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Parents
                </Button>
                </Link>   
            </div>
        </div>
        <div className='inform-body'>
            <p className='idea-content'>Gardening is a great way to educate children about fruits and vegetables and encourage them to spend time outside.
            If you don't have a garden, there are still plenty of ways to get involved - check out local neighbourhood schemes such as allotments or get some pots for the window sill.
            </p>
            <p className='idea-content'> Research suggests children will develop a greater interest in eating healthily if they are involved in the growing of
                                        their own vegetables. Many schools also run their own gardening clubs and so provide an excellent opportunity to increase their engagement with learning and make new friends</p>
            <div className='tab-container2' style={{backgroundImage: `url(${quickVeg})`, alignSelf:'center' }}></div>
            <h3 className='idea-title'>Quick Vegetables</h3>
            <p className='idea-content'> There are many vegetables that are perfect to start your journey towards a green thumb.
            For example, swiss chard, radish, lettuce, courgettes and runner beans are notorious for growing quickly in even the most basic of conditions - all you have to do is remember to water them!
            </p>
            <div className='tab-container2' style={{backgroundImage: `url(${compost})`, alignSelf:'center' }}></div>
            <h3 className='idea-title'>Recycle Waste</h3>
            <p className='idea-content'>Have some old vegetables in the bottom of the drawer that have seen better days? Try recycling these unwanted vegetables into compost to feed your garden.
            This can be a great opportunity to educate your child about decompositon and microbes - who knows? You may spark a passion for biology!</p>
            <div className='tab-container2' style={{backgroundImage: `url(${cupboard})`, alignSelf:'center' }}></div>
            <h3 className='idea-title'>Grow Plants from Food in your Cupboard</h3>
            <p className='idea-content'>To do this all you will need is a sauce or a shallow plastic tray, kitchen roll and veggies. Start by adding a double layer of moist kitchen roll to the bottom of the tray.
            Next slice the tops off some vegetables and place them in the tray. Leave the tray in a windowsill to get some light but make sure the kitchen towels don't dry out.
            Some vegetables will start to sprout after a few days. You may wish to create a scrapbook with your child of the idfferent vegetables and their varying results. </p>
            <div className='tab-container2' style={{backgroundImage: `url(${plantpot})`, alignSelf:'center' }}></div>
            <h3 className='idea-title'>Decorate a Plant Pot</h3>
            <p className='idea-content'>If the weather is preventing you from getting stuck into the gardening outside - try decorating your plant pots using colourful paints. This will help brighten up your garden while also getting the children involved with preperations.</p>
            <div className='tab-container2' style={{backgroundImage: `url(${scrapbook})`, alignSelf:'center' }}></div>
            <h3 className='idea-title'>Create a Garden Scrapbook</h3>
            <p className='idea-content'>Scrapbooks are a wondeful way to keep kids occupied while also creating a nice momento to look back upon. Try adding dried leaves and plants you grown in your garden and creating fact files about each one. Stick in the seed packets of any seeds they grow - this can also be quite useful to remember which plants were successes and which were failures!</p>
            <p className='idea-content'>
                Encourage the kids to get involved with every stage, from planting to harvesting. This can be a great tool to teach children responsibility.
                You could create a water chart to keep track of the plants care and children will enjoy crossing off boxes or placing stickers as a reward.
                When it comes time to harvest your produce, get the kids involved in meal prep and the cooking - check out our 'Meals' section for some great ideas.
            </p>
        </div>
        </div>

    )
    }