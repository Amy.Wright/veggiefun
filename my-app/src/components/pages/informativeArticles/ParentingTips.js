import React from 'react';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import '../../../App.css';
import background from './../images/home-tabsection.jpg';
import vegPerson from './../images/vegPerson.jpeg';
import feltFood from './../images/feltFood.jpg';
import themeFood from './../images/themeFood.jpg';
import alphFood from './../images/vegAlph.jpg';
import './informative.css'

export default function ParentingTips(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Tips and Tricks</h1>
        <p>Having trouble encouraging your child to get their 5-a-day? Try these!</p></div>
        <div className='tab-btns'>
                <Link to='/forparents'>
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>
                Back to Parents  
            </Button>
            </Link>   
        </div>
    </div>
    <div className='inform-body'>
        <p className='idea-content'>A great way to encourage children to eat their vegetables is to include them in preparing them and expose them to the different kinds as much as possible!</p>
        <p className='idea-content'> One way to warm your child up to the idea, is by eating vegetables yourself. Children often learn by modelling their behaviour off of others so make sure ypur eating your 5-a-day too.</p>
        <p className='idea-content'>Another way is to get creative with your vegetable presentation. Try cutting smiley faces into cucumber slices or placing them in a fun bowl.</p>
        <p className='idea-content'>If your child is eating their vegetables remember to praise them! Children's behaviour is often reinforced by rewards and this can often be as simple as a 'Well done!', or invest in some fun stickers and make a chart.</p>
        <div className='tab-container2' style={{backgroundImage: `url(${vegPerson})`, alignSelf:'center' }}></div>
        <h3 className='idea-title'>Create a Vegetable Person</h3>
        <p className='idea-content'>
            Dice some fruit and veg of your choosing and create a person using toothpicks with your child. Encourage them to eat their person before the day is done.
        </p>
        <div className='tab-container2' style={{backgroundImage: `url(${feltFood})`, alignSelf:'center' }}></div>
        <h3 className='idea-title'> Felt Food</h3>
        <p className='idea-content'>This idea mixes arts and crafts with cooking. Head to yoyr local craft store and pick up some squares of colourful felt. Then, alongside your children, cut various
            fruits and vegetables from it. Pick up some sequins and glitter for extra decoration! When preparing your childrens meals, ask them to identify which vegetables and fruits you are preparing using their felt versions.
            This will help educate your children on the fruits and vegetables they eat and help their recognition.
        </p>
        <div className='tab-container2' style={{backgroundImage: `url(${themeFood})`, alignSelf:'center' }}></div>
        <h3 className='idea-title'>Themed Tasting Day</h3>
        <p className='idea-content'>Offer different kinds of healthy food on a special theme day. For example, an 'Orange' day - offer carrots, oranges, pumpkins and apricots.
        
        </p>
        <div className='tab-container2' style={{backgroundImage: `url(${alphFood})`, alignSelf:'center' }}></div>
        <h3 className='idea-title'>Create a Food Alphabet</h3>
        <p className='idea-content'>Create a visual food alphabet wall display. Children can cut out pictures of food and match these to a letter or draw their own impression of the food.</p>
    </div>
    </div>

    )
    }