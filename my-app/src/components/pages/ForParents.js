import React from 'react';
import '../../App.css';
import CardItem from './../CardItem';
import './../Cards.css';
import Button from './../Button';
import { Link } from 'react-router-dom';
import background from './images/home-tabsection.jpg';

export default function ForParents(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Parents</h1>
            <p>Find some ideas for encouraging your child to eat healthy below.</p></div>
            <div className='tab-btns'>
                    <Link to='/'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Home  
                </Button>
                </Link>   
            </div>
        </div>
                    <div className='cards'>
                    <h1>Explore our site by clicking the options below!</h1>
                    <div className='cards-container'>
                        <div className='cards-wrap'>
                            <ul className='cards-items'>
                                <CardItem
                                src="images/home-meals-img.jpg"
                                text="Meals"
                                desc="Check out our collection of yummy meals for your family to try!"
                                label="Health"
                                path='/meals'
                                />
                                <CardItem
                                src="images/activities-homecard.jpg"
                                text="Printable Activities"
                                desc="Check out our printable activities that will keep your child entertained!"
                                label="Family Fun"
                                path='/activities'
                                />
                            </ul>
                            <ul className='cards-items'>
                                <CardItem
                                src="images/tips-homecard.jpg"
                                text="Encouraging Healthy Eating"
                                desc="Tips and Tricks to help you reach your 5-a-day"
                                label="Parenting"
                                path='/tipstricks'
                                />
                                <CardItem
                                src="images/gardening-homecard.jpg"
                                text="Grow Your Own"
                                desc="Gardening is a wonderful way to educate the whole family"
                                label="Gardening"
                                path='/gardening'
                                />
                                 <CardItem
                                src="images/health-homecard.jpg"
                                text="Health Benefits"
                                desc="Discover the health benefits of fruits and vegetables"
                                label="Health"
                                path='/health'
                                />
                            </ul>
                        </div>
                    </div>
                </div>
        </div>   
        )
        }