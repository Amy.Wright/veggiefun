import React from 'react';
import '../../App.css';
import CardItem from './../CardItem';
import './../Cards.css';
import Button from './../Button';
import { Link } from 'react-router-dom';
import background from './images/activities-tabsection.jpg';

export default function Activities(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'> <h1>Activities</h1>
            <p>Get the kids involved with some of our fun activity ideas below.</p></div>
            <div className='tab-btns'>
                    <Link to='/forparents'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Parents  
                </Button>
                </Link>   
            </div>
        </div>
        <div className='cards'>            
            <h1>Activities</h1>
            <div className='cards-container'>
                <div className='cards-wrap'>
                <div>
                    <ul className='cards-items'>
                        <CardItem
                        src="/images/colouringpencils.jpg"
                        text="Colouring Sheets"  
                        desc="Explore your artistic side with our fun colouring sheets"           
                        label="Art"           
                        path='/colouringsheets' />
                        <CardItem
                        src="/images/pens.jpg"
                        text="Crosswords"
                        desc="Test your knowledge with these fun word puzzles"
                        label="Ages 7+"
                        path='/crosswords' />
                        <CardItem
                        src="/images/wordsearches.jpg"
                        text="Word Searches"
                        desc="Try and find all the hidden words inside the wordsearch!"
                        label="Ages 4+"
                        path='/wordsearches' />
                    </ul>

                    </div>
                    </div>
                </div>
        </div>
        </div>
        )
        }