import React, { Component} from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/fruit-skewers.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class FruitSkewers extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Rainbow Fruit Skewers</h1>
            <p>These vitamin-packed fruit skewers are a simple, colourful and fun way to get kids to eat fruit. They'll love helping to make them too.</p>
            <p>Prep: 15 mins</p>
            <p>Serves: 7</p></div>
            <div className='rectab-btns'>
            <Link to='/snacks'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Snacks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>7 raspberries</p>
                <span>------------------</span>
                <p>7 hulled strawberries</p>
                <span>------------------</span>
                <p>7 tangerine segments</p>
                <span>------------------</span>
                <p>7 cubes peeled mango</p>
                <span>------------------</span>
                <p>7 peeled pineapple chunks</p>
                <span>------------------</span>
                <p>7 peeled kiwi fruit chunks</p>
                <span>------------------</span>
                <p>7 green grapes</p>
                <span>------------------</span>
                <p>7 red grapes</p>
                <span>------------------</span>
                <p>14 blueberries</p>
                <span>------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Take 7 wooden skewers and thread the following fruit onto each – 1 raspberry, 1 hulled strawberry, 1 tangerine segment, 1 cube of peeled mango, 1 chunk of peeled pineapple, 1 chunk of peeled kiwi, 1 green and 1 red grape, and finish off with 2 blueberries. Arrange in a rainbow shape and let everyone help themselves.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default FruitSkewers