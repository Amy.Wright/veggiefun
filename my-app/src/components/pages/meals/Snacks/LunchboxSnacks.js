import React, {Component} from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/lunchbox-snacks.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class LunchboxSnacks extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Lunchbox Snacks</h1>
            <p>Keep hunger pangs at bay with these healthy, yummy lunchbox nibbles</p>
            <p>Prep: 30 mins</p>
            <p>Serves: 1</p></div>
            <div className='rectab-btns'>
            <Link to='/snacks'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Snacks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>bean dip with veggie sticks</p>
                <span>-------------------</span>
                <p>raspberry banana smoothie</p>
                <span>-------------------</span>
                <p>cinnamon custard plums</p>
                <span>-------------------</span>
                <p>apricot yogurt granola pots</p>
                <span>-------------------</span>
                <p>carrot & pineapple muffins (all ingredients for recipes below)</p>
                <span>-------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <h5>Bean Dip with Veggie Sticks</h5>
                <p> 5 of 5-a-day, low fat. Whizz 215g can drained butterbeans, squeeze lemon juice, 1 small crushed garlic clove, 1 tbsp each chopped parsley and mint, 2 tsp olive oil and 1 tbsp water. Serve with vegetable dippers made from 1 celery stick, 1 carrot and ½ red pepper - wrap in a damp piece of kitchen paper to stop them drying out (serves 1).</p>
                <h4>Step 2</h4>
                <h5>Raspberry Banna Smoothie</h5>
                <p>Whizz 85g raspberries together with 1 chopped banana and 150ml orange juice until smooth (serves 1).</p>
                <h4>Step 3</h4>
                <h5>Cinnamon Custard Plums</h5>
                <p>Put 1 tbsp honey, 1 tsp butter, 2 quartered plums, few drops vanilla extract, pinch ground cinnamon and 2 tsp water in a dish. Cover with cling film, pierce, microwave for 3 mins on High. Once cooled, put fruit in a container and spoon 2-3 tbsp custard on top. Seal and refrigerate (serves 1).</p>
                <h4>Step  4</h4>
                <h5>Apricot Yoghurt Granola Pots</h5>
                <p>Mix 200g oats, 2 tbsp honey and 1 tbsp sunflower oil in a baking tray. Spread out and bake at 200C/180C fan/ gas 6 for 7 mins. Stir, bake for 7 mins more. Cool. Mix with 150g dried fruits (enough for 8 servings). For one serving, mix 100ml natural yogurt with 1 tbsp sugar-free apricot jam in a container. Store granola in an airtight container and portion into a small bag when packing lunchbox.</p>
                <h4>Step 5</h4>
                <h5>Carrot and Pineapple Muffins</h5>
                <p>Sift together 140g self-raising flour, 85g wholemeal flour (reserving about 2 tbsp of the bran), ½ tsp bicarbonate of soda, 2 tsp ground cinnamon and a pinch salt. In another bowl, beat 150ml sunflower oil with 100g golden caster sugar. Add 200g mashed cooked carrots, 3 canned pineapple slices, cut into cubes, 2 tbsp pineapple juice from the can, 1 egg and 1 tsp vanilla extract. Mix in 50g sunflower seeds. Fold dry mix into the wet one. Cut out a dozen 10cm squares of baking parchment and place in the holes of a muffin tin. Spoon mixture into tin, sprinkle with bran and a few sunflower seeds. Bake at 200C/ 180C fan/gas 6 for 20-25 mins or until a skewer comes out clean. Leave to cool (makes 12).</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default LunchboxSnacks