import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/flapjacks.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class Flapjacks extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1 className='header'>Healthier Flapjacks</h1>
            <p>A healthier version of traditional fruit and nut bars packed with seeds, oats and agave syrup – perfect for snacking and lunchboxes</p>
            <p>Prep: 10 mins Cook: 20 min</p>
            <p>Serves: 12</p></div>
            <div className='rectab-btns'>
            <Link to='/snacks'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Snacks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>150g ready-to-eat stoned dates</p>
                <span>-----------------------------------------</span>
                <p>100g low-fat spread</p>
                <span>-----------------------------------------</span>
                <p>3 generous tbsp agave syrup</p>
                <span>-----------------------------------------</span>
                <p>50g ready-to-eat stoned dried apricots, finely chopped</p>
                <span>-----------------------------------------</span>
                <p>50g chopped toasted hazelnuts</p>
                <span>-----------------------------------------</span>
                <p>3 tbsp mixed seeds</p>
                <span>-----------------------------------------</span>
                <p>50g raisins</p>
                <span>-----------------------------------------</span>
                <p>150g porridge oats</p>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Heat the oven to 190C/170C fan/gas 5. Line an 18cm square tin with baking parchment. Put the dates into a food processor and blitz until they are finely chopped and sticking together in clumps.</p>
                <h4>Step 2</h4>
                <p>Put the low-fat spread, agave syrup and dates into a saucepan and heat gently. Stir until the low-fat spread has melted and the dates are blended in. Add all the remaining ingredients to the pan and stir until well mixed. Spoon the mixture into the tin and spread level.</p>
                <h4>Step 3</h4>
                <p>Bake in the oven for 15-20 mins until golden brown. Remove and cut into 12 pieces. Leave in the tin until cold. Store in an airtight container.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}
}
export default Flapjacks