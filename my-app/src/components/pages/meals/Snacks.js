import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from '../images/snacks-tabsection.jpg'

export default function Snacks(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Snacks</h1>
            <p>Need a quick energy boost? Pick from our selection of healthy snacks.</p></div>
            <div className='tab-btns'>
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>    
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/fruit-skewers.jpg"
                    text="Rainbow Fruit Skewers"
                    label="15 mins"
                    path='/fruit-skewers' />
                    <CardItem
                    src="images/flapjacks.jpg"
                    text="Healthier Flapjacks"
                    label="30 mins"
                    path='/flapjacks' />
                    <CardItem
                    src="images/lunchbox-snacks.jpg"
                    text="Lunchbox Snacks"
                    label="30 mins"
                    path='/lunchbox-snacks' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}