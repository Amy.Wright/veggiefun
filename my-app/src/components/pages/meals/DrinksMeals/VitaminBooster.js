import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/vitamin-booster.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class VitaminBooster extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1 style={{fontSize: 70}}>Orange Vitamin Boosting Smoothie</h1>
            <p>Up your vitamin quota with help from this bright and fresh smoothie. Orange, carrot, celery and mango pack a nutritious punch</p>
            <p>Prep: 5 mins</p>
            <p>Serves: 1</p></div>
            <div className='rectab-btns'>
            <Link to='/drinks'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Drinks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>1 orange , peeled and roughly chopped</p>
                <span>----------------------------------------------------------------</span>
                <p>1 large carrot , peeled and roughly chopped</p>
                <span>----------------------------------------------------------------</span>
                <p>2 sticks celery , roughly chopped</p>
                <span>----------------------------------------------------------------</span>
                <p>50g mango , roughly chopped</p>
                <span>----------------------------------------------------------------</span>
                <p>200ml water</p>
                <span>----------------------------------------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Put all the orange, carrot, celery and mango in the blender, top up with water, then blitz until smooth.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default VitaminBooster