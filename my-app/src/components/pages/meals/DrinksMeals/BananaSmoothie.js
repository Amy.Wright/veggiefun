import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/bananasmoothie.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class BananaSmoothie extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1 style={{fontSize: 70}}>Banana, Clementine and Mango Smoothie</h1>
            <p>Kick-start your day with a drink rich in vitamin C and one of your 5-a-day</p>
            <p>Prep: 15-25 mins</p>
            <p>Serves: 6</p></div>
            <div className='rectab-btns'>
            <Link to='/drinks'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Drinks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>about 24 juicy clementines , plus an extra one for decoration</p>
                <span>------------------------------------------</span>
                <p>2 small, very ripe and juicy mangoes</p>
                <span>------------------------------------------</span>
                <p>2 ripe bananas</p>
                <span>------------------------------------------</span>
                <p>500g tub whole milk or low-fat yogurt</p>
                <span>------------------------------------------</span>
                <p>handful of ice cubes (optional)</p>
                <span>------------------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Halve the clementines and squeeze out the juice – you should have about 600ml/1 pint. (This can be done the night before.) Peel the mangoes, slice the fruit away from the stone in the centre, then chop the flesh into rough pieces. Peel and slice the bananas.</p>
                <h4>Step 2</h4>
                <p>Put the clementine juice, mango flesh, bananas, yogurt and ice cubes into a liquidiser and blend until smooth. Pour into six glasses and serve. (You might need to make this in two batches, depending on the size of your liquidiser.) If you don’t add ice cubes, chill in the fridge until ready to serve.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default BananaSmoothie