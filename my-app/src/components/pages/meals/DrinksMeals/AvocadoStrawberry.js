import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/avocadostrawberry.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class AvocadoStrawberry extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1 style={{fontSize: 70}}>Avocado and Strawberry Smoothie</h1>
            <p>A creamy breakfast-friendly blend that's high in calcium and low in calories</p>
            <p>Prep: 5 mins</p>
            <p>Serves: 2</p></div>
            <div className='rectab-btns'>
            <Link to='/drinks'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Drinks  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>½ avocado , stoned, peeled and cut into chunks</p>
                <span>-----------------------------------------------------------------</span>
                <p>150g strawberry , halved</p>
                <span>-----------------------------------------------------------------</span>
                <p>4 tbsp low-fat natural yogurt</p>
                <span>-----------------------------------------------------------------</span>
                <p>200ml semi-skimmed milk</p>
                <span>-----------------------------------------------------------------</span>
                <p>lemon or lime juice , to taste</p>
                <span>-----------------------------------------------------------------</span>
                <p>honey , to taste</p>
                <span>-----------------------------------------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Put all the ingredients in a blender and whizz until smooth. If the consistency is too thick, add a little water.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default AvocadoStrawberry