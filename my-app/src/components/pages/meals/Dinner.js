import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from '../images/dinner-tabsection.jpg'

export default function Dinner(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Dinner</h1>
            <p>Get inspired by our nutritious, healthy dinner ideas.</p></div>
            <div className='tab-btns'>
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>  
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/chickenvegbowl.jpg"
                    text="Chicken and Vegetable Bowl"
                    label="30 mins"
                    path='/chicken-veg-bowl' />
                    <CardItem
                    src="images/pastasauce.jpg"
                    text="Pasta with Vegetable Sauce"
                    label="65 mins"
                    path='/pasta-and-sauce' />
                    <CardItem
                    src="images/roastdinner.jpg"
                    text="Healthy Roast Dinner and Veg"
                    label="65 mins"
                    path='/roast-dinner' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}