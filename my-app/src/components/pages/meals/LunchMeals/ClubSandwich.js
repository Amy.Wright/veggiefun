import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/clubsandwich.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class ClubSandwich extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Healthy Green Club Sandwich</h1>
            <p>This healthy sandwich is packed full of goodness to keep you going until dinner.</p>
            <p>Prep: 10 mins</p>
            <p>Serves: 1</p></div>
            <div className='rectab-btns'>
            <Link to='/lunch'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Lunches  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                 <p>   3 slices wholegrain or rye bread</p>
                <span>--------------------------------------------</span>
                <p>3 tbsp ready-made hummus</p>
                <span>--------------------------------------------</span>
                <p>1 small avocado (100g), stoned and sliced</p>
               <span> --------------------------------------------</span>
                <p>1 handful rocket leaves </p>
                <span>--------------------------------------------</span>
               <p> 8-12 cherry tomatoes, sliced </p>
                <span>--------------------------------------------</span>
                
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Toast the bread and spread hummus evenly over one side of each slice. On one slice of bread, lay half the avocado, rocket and tomato. Season with pepper, then cover with another slice.</p>
                <h4>Step 2</h4>
                <p>Pile on the rest of the avocado, rocket and tomato, season again and top with the third slice.

</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default ClubSandwich