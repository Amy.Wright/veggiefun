import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/vegsoup.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class VegSoup extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Mel's Hearty Vegetable Soup</h1>
            <p>This warming vegetable soup, from Great British Bake Off host Mel, is a traditional family recipe that packs in fresh veg and spices, making a healthy supper.</p>
            <p>Prep: 30 mins</p>
            <p>Serves: 4</p></div>
            <div className='rectab-btns'>
            <Link to='/lunch'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Lunches  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>200g sourdough bread, cut into croutons</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 tbsp caraway seeds</p>
                <span>-------------------------------------------------------------------</span>
                <p>3 tbsp olive oil</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 garlic clove, chopped</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 carrot, chopped</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 potato, chopped</p>
                <span>-------------------------------------------------------------------</span>
                <p>600ml vegetable stock (we use bouillon)</p>
                <span>-------------------------------------------------------------------</span>
                <p>100g cherry tomatoes, halved</p>
                <span>-------------------------------------------------------------------</span>
                <p>400g can chopped tomatoes</p>
                <span>-------------------------------------------------------------------</span>
                <p>pinch of golden caster sugar</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 celery stick, chopped</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 bouquet garni (2 bay leaves, 1 rosemary and 2 thyme sprigs tied together with string)</p>
                <span>-------------------------------------------------------------------</span>
                <p>200g cauliflower, cut into florets</p>
                <span>-------------------------------------------------------------------</span>
                <p>150g white cabbage, shredded</p>
                <span>-------------------------------------------------------------------</span>
                <p>1 tsp Worcestershire sauce</p>
                <span>-------------------------------------------------------------------</span>
                <p>2 tsp mushroom ketchup</p>
                <span>-------------------------------------------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Heat oven to 180C/160C fan/gas 4. Put the bread on a baking tray with the caraway seeds, half the oil and some sea salt, and bake for 10-15 mins or until golden and crisp. Set aside.</p>
                <h4>Step 2</h4>
                <p>Meanwhile, heat the remaining oil in a large saucepan over a medium heat. Add the garlic, carrot and potato and cook for 5 mins, stirring frequently, until a little softened.

</p>
                <h4>Step 3</h4>
                <p>Add the stock, tomatoes, sugar, bouquet garni, celery and seasoning and bring to a rolling boil. Reduce the heat, simmer for 10 mins, then add the cauliflower and cabbage. Cook for 15 mins until the veg is tender.</p>
                <h4>Step 4</h4>
                <p>Stir in the Worcestershire sauce and mushroom ketchup. Remove the bouquet garni and serve the soup in bowls with the caraway croutons.

</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default VegSoup