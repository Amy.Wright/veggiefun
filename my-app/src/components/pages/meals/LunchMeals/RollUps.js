import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/rollups.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class RollUps extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Carrot and Hummus Roll-Ups</h1>
            <p>Roll-up, roll-up - snaffle something superhealthy in a flash with these vegetarian wraps.</p>
            <p>Prep: 10 mins</p>
            <p>Serves: 4</p></div>
            <div className='rectab-btns'>
            <Link to='/lunch'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Lunches  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>    200g tub hummus</p>
                <span>----------------------------</span>
               <p> 4 seeded wraps</p>
                <span>----------------------------</span>
                <p>4 carrots</p>
                <span>----------------------------</span>
                <p>small handful rocket leaves </p>
                <span>----------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Spread the hummus between wraps. Coarsely grate carrots and scatter on top of the hummus, finishing each wrap with a small handful of rocket leaves and some seasoning. Roll up and eat.</p>
                  </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default RollUps