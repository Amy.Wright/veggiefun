import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from '../images/lunch-tabsection.jpg'

export default function Lunch(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Lunch</h1>
            <p>We've got plenty of healthy lunch ideas to keep your midday meal on the right track.</p></div>
            <div className='tab-btns'>
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>  
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/rollups.jpg"
                    text="Carrot and Hummus Roll-Ups"
                    label="10 mins"
                    path='/roll-ups' />
                    <CardItem
                    src="images/clubsandwich.jpg"
                    text="Healthy Green Club Sandwich "
                    label="10 mins"
                    path='/club-sandwich' />
                    <CardItem
                    src="images/vegsoup.jpg"
                    text="Mel's Hearty Vegetable Soup"
                    label="40 mins"
                    path='/veg-soup' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}