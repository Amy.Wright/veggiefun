import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from './../images/breakfast-tabsection.jpg';

export default function Breakfast(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Breakfast</h1>
            <p>The most important meal of the day - explore our recipes below for a great start.</p></div>
            <div className='tab-btns'>                
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>  
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/overnight-oats.jpg"
                    text=" Overnight Oats"
                    label="10 mins"
                    path='/overnightoats' />
                    <CardItem
                    src="images/pancakes.jpg"
                    text="Healthy Pancakes"
                    label="45 mins"
                    path='/pancakes' />
                    <CardItem
                    src="images/breakfast-burrito.jpg"
                    text="Breakfast Burrito"
                    label="15 mins"
                    path='/breakfastburrito' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}