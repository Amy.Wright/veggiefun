import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from '../images/drinks-tabsection.jpg'

export default function Drinks(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Drinks</h1>
            <p>Make your own nutritious smoothies, these healthier drinks are full of flavour.

</p></div>
            <div className='tab-btns'>
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>  
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/bananasmoothie.jpg"
                    text="Banana, Clementine and Mango Smoothie"
                    label="15 mins"
                    path='/banana-smoothie' />
                    <CardItem
                    src="images/vitamin-booster.jpg"
                    text="Orange Vitamin Boosting Smoothie"
                    label="5 mins"
                    path='/vitamin-booster' />
                    <CardItem
                    src="images/avocadostrawberry.jpg"
                    text="Avocado and Strawberry Smoothie"
                    label="5 mins"
                    path='/avocado-straw-smoothie' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}