import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/pavlova.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class Pavlova extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Lighter Summer Pavlova</h1>
            <p>A combination of orange, satsuma and carrot make these refreshing lollies a low-calorie treat. They're also vegan, gluten-free and sure to be a hit with kids!</p>
            <p>Prep: 35 mins, Cook: 1 hour</p>
            <p>Serves: 8</p></div>
            <div className='rectab-btns'>
            <Link to='/dessert'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Desserts 
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <h4>For the meringue</h4>
                <p>1 tsp cornflour</p>
                <span>------------------</span>
                <p>½ tsp vanilla extract</p>
                <span>------------------</span>
                <p>3 large egg whites , at room temperature</p>
                <span>------------------</span>
                <p>75g golden caster sugar</p>
                <span>------------------</span>
                <p>50g icing sugar</p>
                <span>------------------</span>
                <p>edible red food colouring (optional)</p>
                <span>------------------</span>
                <h4>For the filling</h4>
                <p>100ml whipping cream</p>
                <span>------------------</span>
                <p>200g 2% Greek yogurt</p>
                <span>------------------</span>
                <p>85g half-fat crème fraîche</p>
                <span>------------------</span>
                <p>2 tsp golden caster sugar</p>
                <span>------------------</span>
                <p>350g strawberries , hulled and sliced</p>
                <span>------------------</span>
                <p>150g punnet raspberries</p>
                <span>------------------</span>
                <p>150g punnet blueberries</p>
                <span>------------------</span>
                <p>2 passion fruits</p>
                <span>------------------</span>
                <p>1/8 tsp icing sugar , for sifting</p>
                <span>------------------</span>
                <p>a few small mint leaves (optional)</p>
                <span>------------------</span>
               </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Heat the oven to 150C/130C fan/gas 2. Cover a large baking sheet with baking parchment and use a pencil to draw a 20cm circle in the middle. Turn the parchment over.</p>
                <h4>Step 2</h4>
                <p>For the meringue, mix together the cornflour, vinegar and vanilla, and set aside. Whisk the egg whites in a large bowl until the mixture stands in stiff peaks. Tip in 1 tsp of the caster sugar, beat for a few seconds, then carry on adding and whisking in the caster sugar, 1 tsp at a time – this will give you a thick, glossy mixture. Sift and gently fold in half the icing sugar with a large metal spoon. Add the rest of the icing sugar in the same way, being careful not to overmix. Fold in the reserved cornflour mixture.</p>
                <h4>Step 3</h4>
                <p>Secure the parchment with some meringue mixture (see tip) if you want. Dollop a couple of big spoonfuls of the mixture into the middle of the circle on the baking parchment, then spread it out gently to make a 15cm circular base, about 1.5cm thick. (At this point you can give the remaining meringue mixture a pink ripple effect, if you like, by briefly swirling some food colouring through it with a knife.)</p>
                <h4>Step 4</h4>
                <p>Place a large spoonful of meringue on the edge of your meringue base to come out to the edge of the pencil circle. Repeat with 7 more spoonfuls in the same way, creating the raised edging for the pavlova and leaving a wide hollow for the fruit and cream to go in later. Fill any gaps in the edging with a blob of meringue. Bake for 1 hr – if you gently tap the meringue, it should sound crisp. Turn off the oven but leave the meringue inside for 1 hr to cool gradually. Remove, carefully peel off the baking parchment and leave to cool completely on a wire rack. Can be made up to three days ahead and stored in an airtight tin, or wrapped in foil.</p>
                <h4>Step 5</h4>
                <p>When you are ready to serve the pavlova, prepare the filling. Whisk the whipping cream to stiff peaks. Fold in the Greek yogurt, then the crème fraîche and sugar. Scatter half the strawberries, raspberries and blueberries onto the base of the meringue. Cut the passion fruits in half widthways, scoop out the pulp and seeds, and scatter over the berries. Spoon about half of the yogurt mixture on top. Scatter over some of the remaining berries, top this with more spoonfuls of the yogurt mix, then finish off with the rest of the berries. Dust lightly with icing sugar and a scattering of mint leaves, if you like. Serve straight away – if you leave it sitting for too long, the creamy filling and juicy fruits will cause the meringue to start to soften.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
    }}
    export default Pavlova