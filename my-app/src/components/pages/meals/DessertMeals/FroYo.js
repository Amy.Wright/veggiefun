import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/froyo.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class FroYo extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Strawberry Frozen Yoghurt</h1>
            <p>Our easy strawberry frozen yogurt is intensely fruity and really creamy, perfect for a simple summer sweet treat</p>
            <p>Prep: 10 mins</p>
            <p>Serves: 5</p></div>
            <div className='rectab-btns'>
            <Link to='/dessert'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Desserts 
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>140g strawberries</p>
                <span>------------------------------</span>
                <p>½ x 405g can light condensed milk</p>
                <span>------------------------------</span>
                <p>500g tub 0%-fat Greek yogurt</p>
                <span>------------------------------</span>
               </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Roughly chop half the strawberries and whizz the rest in a food processor or with a stick blender to a purée.</p>
                <h4>Step 2</h4>
                <p>In a big bowl, stir the condensed milk into the puréed strawberries then gently stir in the yogurt until well mixed. Fold through the chopped strawberries.</p>
                <h4>Step 3</h4>
                <p>Scrape the mixture into a loaf tin or container, pop on the lid or wrap well in cling film and freeze overnight, until solid. Remove from the freezer about 10-15 mins before you want to serve the frozen yogurt. Can be frozen for up to 1 month.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default FroYo