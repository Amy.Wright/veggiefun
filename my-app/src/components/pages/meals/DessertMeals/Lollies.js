import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/lollies.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class Lollies extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Veganuary Orange Lollies</h1>
            <p>A combination of orange, satsuma and carrot make these refreshing lollies a low-calorie treat. They're also vegan, gluten-free and sure to be a hit with kids!</p>
            <p>Prep: 20 mins</p>
            <p>Serves: 6</p></div>
            <div className='rectab-btns'>
            <Link to='/dessert'>    
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Desserts 
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>5 large carrots</p>
                <span>---------------------------</span>
                <p>juice of 3 large oranges , zest of 1</p>
                <span>---------------------------</span>
                <p>1 satsuma , peeled then chopped (optional)</p>
                <span>---------------------------</span>
               </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Finely grate the carrots and place in the middle of a clean tea towel. Gather up the towel, and squeeze the carrot juice into a jug, discarding the pulp. Add the orange juice and top up with a little cold water if needed to make up 360ml liquid. Stir in the orange zest and satsuma pieces, if using. Pour into lolly moulds and freeze overnight.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default Lollies