import React, { Component }from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/overnight-oats.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class OvernightOats extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Overnight Oats</h1>
            <p>Follow this recipe for easy overnight oats. Try dried fruit, seeds and nuts, grated apple or pear, or chopped tropical fruits to reach your 5-a-day.</p>
            <p>Prep: 10 mins</p>
            <p>Serves: 1</p></div>
            <div className='rectab-btns'>
            <Link to='/breakfast'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Breakfasts  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>1/4 tsp ground cinnamon</p>
                <span>----------------------------------</span>
                <p>50g rolled porridge oats</p>
                <span>----------------------------------</span>
                <p>2 tbsp natural yoghurt</p>
                <span>----------------------------------</span>
                <p>50g mixed berries </p>
                <span>---------------------------------</span>
                <p>drizzle of honey </p>
                <span>----------------------------------</span>
                <p>1/2 tbsp nut butter</p>
                <span>----------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>The night before serving, stir the cinnamon and 100ml water (or milk) into your oats with a pinch of salt. </p>
                <h4>Step 2</h4>
                <p>The next day, loosen with a little more water (or milk) if needed. Top with the yogurt, berries, a drizzle of honey and the nut butter.

</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default OvernightOats