import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/pancakes.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class Pancakes extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Healthy Pancakes</h1>
            <p>These easy healthy pancakes get their fluffy texture from whipped egg whites. Stack them high with fresh berries and a spoonful of low-fat yogurt</p>
            <p>Prep: 15 mins, Cook: 30 mins</p>
            <p>Makes: 10-12</p></div>
            <div className='rectab-btns'>
            <Link to='/breakfast'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Breakfasts  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>50g self-raising flour</p>
                <span>------------------</span>
                <p>50g wholemeal or wholegrain flour</p>
                <span>------------------</span>
                <p>2 small eggs, seperated</p>
                <span>------------------</span>
                <p>150ml skimmed milk</p>
                <span>------------------</span>
                <p>berrries and/or low fat yoghurt</p>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Sift the flours into a bowl or wide jug and tip any bits in the sieve back into the bowl. Add the egg yolks and a splash of milk then stir to a thick paste. Add the remaining milk a little at a time so you don’t make lumps in the batter.</p>
                <h4>Step 2</h4>
                <p>Whisk the egg whites until they stand up in stiff peaks, then fold them carefully into the batter – try not to squash out all the air.</p>
                <h4>Step 3</h4>
                <p>Heat a non-stick pan over a medium heat and pour in enough batter to make a pancake about 10 cm across. Cook for just under a minute until bubbles begin to pop on the surface and the edges are looking a little dry. Carefully turn the pancake over. If it is a bit wet on top, it may squirt out a little batter as you do so. In that case, leave it on the other side a little longer. Keep warm while you make the remaining pancakes. Serve with your favourite healthy toppings.</p>
            </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}
}
export default Pancakes