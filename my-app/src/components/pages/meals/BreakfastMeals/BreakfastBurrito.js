import React, { Component }  from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/breakfast-burrito.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class BreakfastBurrito extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Breakfast Burrito</h1>
            <p>Make a nutritious cocoon for breakfast ingredients with a wholemeal wrap. We’ve included protein-rich eggs and avocado to add good fats to this burrito</p>
            <p>Prep: 5 mins,     Cook: 10 mins</p>
            <p>Serves: 1</p></div>
            <div className='rectab-btns'>
            <Link to='/breakfast'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Breakfasts  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>1 tsp chipotle paste</p>
                <span>---------------------------</span>
                <p>1  egg</p>
                <span>---------------------------</span>
                <p>i tsp rapeseed oil</p>
                <span>---------------------------</span>
                <p>50g kale</p>
                <span>---------------------------</span>
                <p>7 cherry tomatoes, halved</p>
                <span>---------------------------</span>
                <p>1/2 small avocado, sliced</p>
                <span>---------------------------</span>
                <p>1 wholemeal tortilla wrap, warmed</p>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Whisk the chipotle paste with the egg and some seasoning in a jug. Heat the oil in a large frying pan, add the kale and tomatoes.</p>
                <h4>Step 2</h4>
                <p>Cook until the kale is wilted and the tomatoes have softened, then push everything to the side of the pan. Pour the beaten egg into the cleared half of the pan and scramble. Layer everything into the centre of your wrap, topping with the avocado, then wrap up and eat immediately.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}
}
export default BreakfastBurrito