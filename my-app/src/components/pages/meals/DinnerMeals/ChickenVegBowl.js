import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/chickenvegbowl.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class ChickenVegBowl extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Chicken and Vegetable Bowl</h1>
            <p>Make this colourful chicken, brown rice and vegetable dish for the whole family. With avocado, edamame beans, sweetcorn and carrots, it's not only tasty, but healthy too.</p>
            <p>Prep: 15 mins, Cook: 15 mins</p>
            <p>Serves: 4</p></div>
            <div className='rectab-btns'>
            <Link to='/dinner'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Dinners  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>250g brown basmati rice</p>
                <span>-------------------------</span>
                <p>1 tbsp rapeseed oil</p>
                <span>-------------------------</span>
                <p>1 garlic clove , crushed</p>
                <span>-------------------------</span>
                <p>2 chicken breasts, sliced</p>
                <span>-------------------------</span>
                <p>2 tbsp hoisin sauce</p>
                <span>-------------------------</span>
                <p>100g frozen edamame beans or peas, defrosted</p>
                <span>-------------------------</span>
                <p>100g frozen sweetcorn</p>
                <span>-------------------------</span>
                <p>100g grated carrots</p>
                <span>-------------------------</span>
                <p>100g red peppers , cut into small cubes</p>
                <span>-------------------------</span>
                <p>1 avocado , stoned and sliced</p>
                <span>-------------------------</span>
                <p>1 lemon , cut into quarters, to serve (optional)</p>
                <span>-------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Cook the rice following pack instructions, then drain and return to the pan to keep warm. Heat the oil in a frying pan or wok, add the garlic and fry for 2 mins or until golden. Tip in the chicken and fry until the pieces are cooked through, then stir in the hoisin sauce, season and continue cooking for a further 2 mins. Cook the edamame beans and sweetcorn in simmering water for 2 mins, then drain.</p>
                <h4>Step 2</h4>
                <p>Divide the rice between four bowls and top with the chicken slices in a strip down the middle, with the carrot, red pepper, beans or peas, sweetcorn and avocado down either side. Serve with the lemon to squeeze over, if you like.

</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default ChickenVegBowl