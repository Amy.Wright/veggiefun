import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/roastdinner.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class RoastDinner extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'> <h1>Healthy Roast Dinner and Veg</h1>
            <p>Cook a healthy, low-fat version of roast beef for two, made with lean, juicy steak and roasted veg, plus gravy on the side.</p>
            <p>Prep: 15 mins, Cook: 50 mins</p>
            <p>Serves: 2</p></div>
            <div className='rectab-btns'>
            <Link to='/dinner'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Dinners  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>285g medium potatoes, thickly sliced</p>
                <span>--------------------------</span>
                <p>4 small carrots (160g), halved lengthways</p>
                <span>--------------------------</span>
                <p>2 x 80g red onions, cut into quarters</p>
                <span>--------------------------</span>
                <p>170g large Brussels sprouts (about 8-10), trimmed</p>
                <span>--------------------------</span>
                <p>2½ tsp rapeseed oil</p>
                <span>--------------------------</span>
                <p>2 tsp thyme leaves</p>
                <span>--------------------------</span>
                <p>2 tsp balsamic vinegar</p>
                <span>--------------------------</span>
                <p>1 large garlic clove, finely grated</p>
                <span>--------------------------</span>
                <p>2 pinches of English mustard powder</p>
                <span>--------------------------</span>
                <p>170g thick, lean fillet steak</p>
                <span>--------------------------</span>
                <p>½ tsp vegetable bouillon powder</p>
                <span>--------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Heat the oven to 180C/160C fan/gas 4. Bring a large pan of water to the boil and cook the potatoes for 5 mins. Drain, reserving the water.</p>
                <h4>Step 2</h4>
                <p>Toss the potatoes, carrots, onions and sprouts with 2 tsp of the oil to coat. Arrange on a non-stick baking sheet, spaced apart. Scatter with 1 tsp of the thyme, grind over some black pepper, then roast for 30 mins.</p>
                <h4>Step 3</h4>
                <p>Meanwhile, mix 1 tsp of the vinegar with the garlic, remaining thyme and oil, the mustard and plenty of black pepper. Rub this over the steak, put in a shallow dish and set aside. Mix the rest of the vinegar with the bouillon and 125ml of the reserved water from step 1, then set aside. After 30 mins, turn the veg over and roast for 15 mins more.</p>
                <h4>Step 4</h4>
                <p>Meanwhile, heat a small non-stick frying pan over a medium-high heat. Lift the steak out of the marinade, shake off the excess and fry for 2-3 mins on each side until cooked to your liking. Remove to a board and leave to rest. Pour the leftover marinade into the frying pan and bubble until thickened slightly to make a gravy. Slice the steak and serve with the roast veg and gravy on the side.</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default RoastDinner