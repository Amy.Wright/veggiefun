import React, { Component } from 'react';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import './../../../RecipesTabSection.css';
import '../../../Recipes.css';
import background from '../../images/pastasauce.jpg'
import CommentBox from '../../../comments/CommentBox';

const imagesPath = {
    heartOutline: './../../images/heart outline.png',
    heartFilled: './../../images/heart_filled.png'
  }

export class PastaSauce extends Component{
    state = {
        open: true
      }
      toggleImage = () => {
        this.setState(state => ({ open: !state.open }))
      }
    
      getImageName = () => this.state.open ? 'heartOutline' : 'heartFilled'
  render(){
    const imageName = this.getImageName();
    return (
        <div className='backColour'>
            <div className='rectab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='rectrans-box'><h1>Pasta with Vegetable Sauce</h1>
            <p>Get your family to eat more veg with this super healthy pasta sauce recipe which counts as 5 of your 5-a-day. The sauce is freezable too
</p>
            <p>Prep: 15 mins, Cook: 50 mins</p>
            <p>Serves: 4</p></div>
            <div className='rectab-btns'>
            <Link to='/dinner'>   
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Dinners  
                </Button>
                </Link>
                </div>
            </div>
            <div className='body'>
            <div className="ingredients">
                <h3>Ingredients</h3>
                <p>1 tsp olive oil</p>
                <span>----------------------------------</span>
                <p>1 large onion, chopped</p>
                <span>----------------------------------</span>
                <p>2 celery sticks, chopped</p>
                <span>----------------------------------</span>
                <p>2 carrots, chopped</p>
                <span>----------------------------------</span>
                <p>1 leek, chopped</p>
                <span>----------------------------------</span>
                <p>2 peppers, deseeded and chopped</p>
                <span>----------------------------------</span>
                <p>2 cans chopped tomatoes with garlic</p>
                <span>----------------------------------</span>
                <p>1 tbsp each caster sugar and balsamic vinegar</p>
                <span>----------------------------------</span>
                <p>300g dried pasta shapes</p>
                <span>----------------------------------</span>
                <p>parmesan, shaved, and rocket, to serve (optional)</p>
                <span>-----------------------------------</span>
            </div>
            <div className="method">
                <h3>Method</h3>
                <h4>Step 1</h4>
                <p>Heat the oil in a large non-stick saucepan and gently cook the onion, celery, carrots and leek until soft, about 20 mins. Add the peppers and cook for 10 mins more, then tip in the tomatoes, sugar and vinegar. Simmer for at least 20 mins – the longer the better.</p>
                <h4>Step 2</h4>
                <p>Cook the pasta following pack instructions. Meanwhile, blitz the sauce with a hand blender until smooth, season and return to the heat to keep warm while the pasta cooks. Drain the pasta and toss through the sauce. Serve in bowls topped with shaved Parmesan and rocket leaves, if you like.

</p>
                </div>
            </div>
            <div className='like-section'>
            <img style={{maxWidth: '50px'}} src={imagesPath[imageName]} onClick={this.toggleImage} />
            </div>
            <div className='comment-section'>
                <CommentBox currentUserId="1"/>
            
            </div>
         </div>
    )
}}
export default PastaSauce