import React from 'react';
import './../../TabSection.css';
import Button from './../../Button';
import { Link } from 'react-router-dom';
import CardItem from './../../CardItem';
import './../../Cards.css';
import background from '../images/dessert-tabsection.jpg'

export default function Dessert(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Dessert</h1>
            <p>Who said dessert can't be healthy? Treat yourself to our healthy options and still get your 5-a-day.</p></div>
            <div className='tab-btns'>
            <Link to='/meals'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Meals  
                </Button>
            </Link>  
            </div>
        </div>
        <div className='cards'>            
        <h1>Meals</h1>
        <div className='cards-container'>
            <div className='cards-wrap'>
            <div>
                <ul className='cards-items'>
                    <CardItem
                    src="images/froyo.jpg"
                    text="Strawberry Frozen Yoghurt"
                    label="10 mins"
                    path='/frozen-yoghurt' />
                    <CardItem
                    src="images/pavlova.jpg"
                    text="Lighter Summer Pavlova"
                    label="95 mins"
                    path='/pavlova' />
                    <CardItem
                    src="images/lollies.jpg"
                    text="Veganuary Orange Lollies"
                    label="20 mins"
                    path='/lollies' />
                </ul>
                </div>
                </div>
            </div>
    </div>
    </div>
    )
}