
import '../../../../App.css';

export const QuizData = [
    {
        id: 0,
        question: `I am round and red. My meat is white. I am a fruit. I can come in different colours such as green and yellow. Who am I?`,
        options: [`Strawberry`, `Orange`, `Pear`, `Apple`],
        answer: `Apple`
    },
    {
        id: 1,
        question: `You can eat me as a fruit or squeeze me for juice. I have lots of vitamin C and my colour is orange. Who am I?`,
        options: [`Orange`, `Parsnip`, `Strawberry`, `Tomato`],
        answer: `Orange`
    },
    {
        id: 2,
        question: `I come in different colours, like green or purple. Before I became raisins, I was a bunch of these. Who am I?`,
        options: [`Lemon`, `Grapes`, `Banana`, `Broccoli`],
        answer: `Grapes`
    },
    {
        id: 3,
        question: `I am a fruit. I taste sweet. You can serve me on a cake with whipped cream. You can bake me in a pie. Who am I?`,
        options: [`Apple`, `Pears`, `Strawberry`, `Orange`],
        answer: `Strawberry`
    },
    {
        id: 4,
        question: `I am green. I look like a tree. I am a vegetable. Who am I?`,
        options: [`Potato`, `Broccoli`, `Cucumber`, `Lettuce`],
        answer: `Broccoli`
    },
]
