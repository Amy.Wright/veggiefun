import './../Quizzes.css';
import React, { Component } from 'react';
import { QuizData } from './QuizData2';
import './../Quizzes.css';
import Button from './../../../Button';
import { Link } from 'react-router-dom';
import background from './../../images/forKidsBanner.png';

export class RiddleQuiz extends Component{

    constructor(props) {
        super(props)

        this.state = {
            userAnswer:null,
            currentIndex:0,
            options: [],
            quizEnd: false,
            score:0,
            disabled:true
        }
    }

    loadQuiz = () => {
        const {currentIndex} = this.state;
        this.setState(() => {
            return {
                question: QuizData[currentIndex].question,
                options: QuizData[currentIndex].options,
                answer: QuizData[currentIndex].answer
            }
        })
    }

    nextQuestion = () => {
        const {userAnswer, answer, score} = this.state

        if(userAnswer===answer){
            this.setState({
                score: score + 1
            })
        }

        this.setState({
            currentIndex: this.state.currentIndex + 1,
            userAnswer: null
        })
    }

    componentDidMount() {
        this.loadQuiz();
    }

    checkAnswer = answer => {
        this.setState({
            userAnswer: answer,
            disabled: false
        })
    }

    finishHandler =() => {
        if(this.state.currentIndex === QuizData.length -1){
            this.setState({
                quizEnd:true
            })
        }
    }

    componentDidUpdate(prevProps, prevState){
        const {currentIndex} = this.state;
        if(this.state.currentIndex !== prevState.currentIndex) {
            this.setState(() => {
                return {
                    question: QuizData[currentIndex].question,
                    image: QuizData[currentIndex].image,
                    options: QuizData[currentIndex].options,
                    answer: QuizData[currentIndex].answer
                }
            });
        }
    }

    render() {
        const {question, image, options, currentIndex, userAnswer, quizEnd} = this.state
        if(quizEnd) {
            return (
                <div>
                    <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
                    <div className='trans-box'><h1>Guess the Riddle</h1>
        <p>Well done for completing the quiz!</p></div>
        <div className='tab-btns'>
                <Link to='/forkids'>
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>
                Back to Kids  
            </Button>
            </Link>   
        </div>
    </div>
    <div className='end'>
                    <h1>Game Over. Final score is {this.state.score} points</h1>
                    <p>The correct answers for the quiz are</p>
                    <ul className='final-answers'>
                        {QuizData.map((item, index) => (
                            <li className='ui floating message options'
                                key={index}>
                                    {item.answer}
                            </li>
                        ))}
                    </ul>
                    <Link to='/forkids'>
                    <button className='next-button'>
                    
                    Back to Kids  
                </button></Link>
               
                </div>
                </div>
            )
        }
        return (
            <div>
            <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Guess The Riddle</h1>
        <p>Try and guess the vegetable described below!</p></div>
        <div className='tab-btns'>
                <Link to='/forkids'>
            <Button className='btns' buttonStyle='btn--outline'
            buttonSize='btn--large'>
                Back to Kids  
            </Button>
            </Link>   
        </div>
    </div>
        <div className='quizbody'>
            <h2 className='q-title'>{question}</h2>
            <span className='q-no'>{`Question ${currentIndex + 1} of ${QuizData.length}`}</span>
            {
                options.map(option => 
                    <p key= {option.id} className={`options ${userAnswer === option ? "selected" : null}` } 
                    onClick = {() => this.checkAnswer(option)}
                    >
                        {option}
                    </p>
                    )
            }
            {currentIndex < QuizData.length - 1 && 
            <button className='next-button' disabled = {this.state.disabled} onClick={this.nextQuestion}>
                Next Question
            </button>}
            {currentIndex === QuizData.length-1 && 
            <button className='next-button' onClick={this.finishHandler} disabled = {this.state.disabled}>
                Finish
            </button>}
            </div>
        </div>
    )
    }
}

export default RiddleQuiz