
import '../../../../App.css';

export const QuizData = [
    {
        id: 0,
        question: `Which one of these is not orange?`,
        options: [`Carrot`, `Orange`, `Pumpkin`, `Apple`],
        answer: `Apple`
    },
    {
        id: 1,
        question: `Which one of these is not red?`,
        options: [`Lemon`, `Apple`, `Strawberry`, `Tomato`],
        answer: `Lemon`
    },
    {
        id: 2,
        question: `Which one of these is not yellow?`,
        options: [`Lemon`, `Pineapple`, `Banana`, `Broccoli`],
        answer: `Broccoli`
    },
    {
        id: 3,
        question: `Which one of these does not grow on a tree?`,
        options: [`Apple`, `Pears`, `Melon`, `Orange`],
        answer: `Melon`
    },
    {
        id: 4,
        question: `Which of these does not grow in the ground?`,
        options: [`Potato`, `Carrot`, `Onion`, `Tomato`],
        answer: `Tomato`
    },
]
