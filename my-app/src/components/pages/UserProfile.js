import React, { Component } from 'react'
import background from './../pages/images/forKidsBanner.png';
import Button from './../Button';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import {Avatar} from 'antd';
import { UserOutlined } from '@ant-design/icons';
import ProfilePicChanger from '../ProfilePicChanger/ProfilePicChanger';
import pic1 from './images/broccolicardgame.jpg';
import pic2 from './images/carrotcardgame.jpg';
import pic3 from './images/onioncardgame.jpg';
import pic4 from './images/peppercardgame.jpg';
import pic5 from './images/peascardgame.jpg';
import pic6 from './images/tomatocardgame.jpg';
import UserDetails from '../ProfilePicChanger/UserDetails';
import Likes from '../ProfilePicChanger/Likes';
import Certificates from '../ProfilePicChanger/Certificates';
import './../ProfilePicChanger/UserProfile.css';
import likehistory from './../pages/images/likes-section.jpg'



class UserProfile extends Component{
    constructor(props){
        super(props)
        this.state = {
            profileImage: ''
        }
    }

    handleImageChange = (profileImage) => {
        this.setState({
            profileImage
        })
    }
    render() {
    return (
        <div>
           <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
           <div className='trans-box'> <h1>Welcome!</h1>
            <p>Keep track of your vegetable journey here.</p></div>
            <div className='tab-btns'>
                    <Link to='/'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Home  
                </Button>
                </Link>   
            </div>
        </div>
        <div className='userarea'>
        <div className='left'>
        <div className='Avatar'><Avatar size={250} icon={<UserOutlined />} src={this.state.profileImage}/></div>
        <div className='changeAv'><ProfilePicChanger handleImageChange={this.handleImageChange} pic1={pic1} pic2={pic2} pic3={pic3} pic4={pic4} pic5={pic5} pic6={pic6}/></div>
        </div>
        <div className='center'>
            <div className='details'>
        <UserDetails/>
        </div>
        </div>
        <div className='right'>
            <div className='likes'>
        <Likes/>
        </div>
            
               <img className='img-likes' src={likehistory}></img>

        </div>
        <Certificates/> 
        </div>       
        </div>
    )
}
}
export default UserProfile