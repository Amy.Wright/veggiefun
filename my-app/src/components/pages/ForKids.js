import React from 'react';
import '../../App.css';
import CardItem from './../CardItem';
import './../Cards.css';
import Button from './../Button';
import { Link } from 'react-router-dom';
import background from './../pages/images/forKidsBanner.png';

export default function ForKids(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
        <div className='trans-box'><h1>Kids</h1>
            <p>Get involved with our fun games below.</p></div>
            <div className='tab-btns'>
                    <Link to='/'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Home  
                </Button>
                </Link>   
            </div>
        </div>
                    <div className='cards'>
                    <div className='cards-container'>
                        <div className='cards-wrap'>      
                    <ul className='cards-items'>
                        <CardItem
                        src="/images/oddoneout-card.jpg"
                        text="Odd One Out"   
                        desc="Try and choose the vegetable that is the Odd One Out"          
                        label="5 mins"           
                        path='/oddoneoutquiz' />
                        <CardItem
                       src="/images/guess-card.jpg"
                        text="Guess the Riddle"
                        desc="Try and solve the riddle to pick the right vegetable"
                        label="5 mins"
                        path='/riddlequiz' />
                        </ul>  
                        <ul className='cards-items'>
                        <CardItem
                        src="/images/hangmanCard.png"
                        text="Hangman" 
                        desc="Guess the letters to form the vegetable before it is too late"            
                        label="5 mins"           
                        path='/hangman' />
                        <CardItem
                        src="/images/memoryMatchCard.jpg"
                        text="Card Match"  
                        desc="Find the cards that match eachother in as few goes as possible"           
                        label="5 mins"           
                        path='/matchmemory' />
                         <CardItem
                        src="/images/jigsawCard.jpg"
                        text="Jigsaw Puzzle"      
                        desc="Try and solve the jigsaw puzzle to meet Brad the Broccoli"       
                        label="5 mins"           
                        path='/jigsaw' />
                        </ul>    
                        </div>
                    </div>
                </div>
        </div>   
        )
        }