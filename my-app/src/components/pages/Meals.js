import React from 'react';
import '../../App.css';
import CardItem from './../CardItem';
import './../Cards.css';
import Button from './../Button';
import { Link } from 'react-router-dom';
import background from './images/meals-tabsection.jpg';

export default function Meals(){
    return (
        <div>
        <div className='tab-container' style={{backgroundImage: `url(${background})` }}>
            <div className='trans-box'><h1>Meals</h1>
            <p>Explore out tasty recipes below!
            </p></div>
            <div className='tab-btns'>
            <Link to='/forparents'>
                <Button className='btns' buttonStyle='btn--outline'
                buttonSize='btn--large'>
                    Back to Parents  
                </Button>
            </Link>   
            </div>
        </div>
        <div className='cards'>            
            <h1>Meals</h1>
            <div className='cards-container'>
                <div className='cards-wrap'>
                <div>
                    <ul className='cards-items'>
                        <CardItem
                        src="images/breakfast-header.jpg"
                        text="Breakfast"      
                        desc="The most important meal of the day - explore our recipes for a great start"    
                        path='/breakfast' />
                        <CardItem
                        src="images/lunches-header.jpg"
                        text="Lunch"
                        desc="We've got plenty of healthy lunch ideas to keep your midday meal on the right track."
                        path='/lunch' />
                        <CardItem
                        src="images/dinner-header.jpg"
                        text="Dinner"
                        desc="Get inspired by our nutritious, healthy dinner ideas."
                        path='/dinner' />
                    </ul>
                    <ul className='cards-items'>
                        <CardItem
                        src="images/dessert-header.jpg"
                        text="Dessert"
                        desc="Who said dessert can't be healthy? Treat yourself to our healthy options and still get your 5-a-day."
                        path='/dessert' />
                        <CardItem
                        src="images/drinks-header.jpg"
                        text="Drinks"
                        desc="Make your own nutritious smoothies, these healthier drinks are full of flavour."
                        path='/drinks' />
                        <CardItem
                        src="images/snacks-header.jpg"
                        text="Snacks"
                        desc="Need a quick energy boost? Pick from our selection of healthy snacks."
                        path='/snacks' />
                    </ul>
                    </div>
                    </div>
                </div>
        </div>
        </div>
    )
}