import React, {useState} from 'react';
import LoginForm from './LoginForm';
import '../Form.css'
import UserProfile from './UserProfile';
import Proceed from '../Proceed';

const Form2 = () => {
    const [isSubmitted, setIsSubmitted] = useState(false)

    function submitForm() {
        setIsSubmitted(true);
    }
    return (
        <>
        <div className='form-container'>
            <span className="close-btn">X</span>
            <div className='form-content-left'>
                {/*<img src="" alt="" className="form-img />*/}
            </div>
            {!isSubmitted ? (<LoginForm submitForm={submitForm} />) : <Proceed />}
        </div>
        
           
        
        </>
    )
}

export default Form2