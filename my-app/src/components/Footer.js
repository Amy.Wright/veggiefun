import React from 'react';
import Button from './Button';
import './Footer.css';
import { Link } from 'react-router-dom';
import { Axios } from 'axios';
import { useState } from 'react';
import NewsletterForm from './newsletterForm';
import validate from './ValidateForm3'


const Footer = ({submitForm}) => {
    const {handleChange, values, handleSubmit, errors} = NewsletterForm(submitForm, validate);
    return(
        <div className='footer-container'>
            <section className='footer-subscription'>
                <p className='footer-title'>
                    VeggieFun
                </p>
                <p className='footer-text'>
                    Why not sign up for our monthly newsletter or check out our social media below:
                </p>
                <div className='footer-input'>
                    <form onSubmit={handleSubmit}>
                        <p>Newsletter</p>
                        <div>
                            <input 
                            id='email' 
                            type='email' 
                            value={values.email} 
                            onChange={handleChange} 
                            name='email' 
                            placeholder='Enter your email' 
                            className='footer-form' 
                            style={{marginBottom:10}}/>
                            {errors.email && <p>{errors.email}</p>}
                        </div>
                        <Button onClick={handleSubmit} buttonStyle='btn--outline'>Subscribe</Button>                    
                    </form>
                </div>
            </section>
            <div className='footer-links'>
                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <a href='https://www.instagram.com/'>
                            <img src='./images/instagram.png' style={{width:40, height: 40}}/>
                            </a>
                    </div>
                </div>
                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <a href='https://en-gb.facebook.com/'>
                        <img src='./images/facebook.png'style={{width:40, height: 40}}/>
                        </a>
                    </div>
                </div>
                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <a href='https://twitter.com/?lang=en-gb'>
                        <img src='./images/twitter.png'style={{width:40, height: 40}}/>
                        </a>
                    </div>
                </div>
                <div className='footer-link-wrapper'>
                    <div className='footer-link-items'>
                        <a href='https://www.pinterest.co.uk/'>
                        <img src='./images/pinterest.png'style={{width:40, height: 40}}/>
                        </a>
                    </div>
                </div>
            </div>
            
        </div>
    )
}

export default Footer