import {useState, useEffect, useCallback} from 'react';
import Axios from 'axios'

const NewsletterForm = (callback, validate) => {
    const [values, SetValues] = useState({
        email: ''
    })
    const [errors, SetErrors] = useState({})
    const [isSubmitting, setIsSubmitting] =useState(false)

    const handleChange = e => {
        const { name, value} = e.target
        SetValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        SetErrors(validate(values));
        Axios.post('http://localhost:3001/newsletter', (values)).then((response) => console.log(response))
        setIsSubmitting(true);
    }
    return {handleChange, values, handleSubmit, errors}
}

export default NewsletterForm