import React from 'react';
import './App.css';
import Navbar from './components/Navbar.js';
import Footer from './components/Footer';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Home from './components/pages/Home';
import Meals from './components/pages/Meals';
import Activities from './components/pages/Activities';
import Breakfast from './components/pages/meals/Breakfast';
import Dessert from './components/pages/meals/Dessert';
import Dinner from './components/pages/meals/Dinner';
import Drinks from './components/pages/meals/Drinks';
import Lunch from './components/pages/meals/Lunch';
import OvernightOats from './components/pages/meals/BreakfastMeals/OvernightOats';
import Pancakes from './components/pages/meals/BreakfastMeals/Pancakes';
import BreakfastBurrito from './components/pages/meals/BreakfastMeals/BreakfastBurrito';
import ClubSandwich from './components/pages/meals/LunchMeals/ClubSandwich';
import RollUps from './components/pages/meals/LunchMeals/RollUps';
import VegSoup from './components/pages/meals/LunchMeals/VegetableSoup';
import ChickenVegBowl from './components/pages/meals/DinnerMeals/ChickenVegBowl';
import PastaSauce from './components/pages/meals/DinnerMeals/PastaSauce';
import RoastDinner from './components/pages/meals/DinnerMeals/RoastDinner';
import FroYo from './components/pages/meals/DessertMeals/FroYo';
import Lollies from './components/pages/meals/DessertMeals/Lollies';
import Pavlova from './components/pages/meals/DessertMeals/Pavlova';
import BananaSmoothie from './components/pages/meals/DrinksMeals/BananaSmoothie';
import VitaminBooster from './components/pages/meals/DrinksMeals/VitaminBooster';
import AvocadoStrawberry from './components/pages/meals/DrinksMeals/AvocadoStrawberry';
import Snacks from './components/pages/meals/Snacks';
import LunchboxSnacks from './components/pages/meals/Snacks/LunchboxSnacks';
import Flapjacks from './components/pages/meals/Snacks/Flapjacks';
import FruitSkewers from './components/pages/meals/Snacks/FruitSkewers';
import Form from './components/pages/Form';
import Form2 from './components/pages/Form2';
import ColouringSheets from './components/pages/activitySheets/ColouringSheets';
import CrossWords from './components/pages/activitySheets/Crosswords';
import Wordsearches from './components/pages/activitySheets/Wordsearches';
import Gardening from './components/pages/informativeArticles/Gardening';
import HealthBenefits from './components/pages/informativeArticles/HealthBenefits';
import ParentingTips from './components/pages/informativeArticles/ParentingTips';
import VegColour from './components/pages/activitySheets/ColouringSheets/VegColour';
import VegCrossword from './components/pages/activitySheets/Crosswords/VegCrossword';
import VegWord from './components/pages/activitySheets/Wordsearches/VegWord';
import OddOneOutQuiz from './components/pages/Quizzes/OddOneOut/OddOneOutQuiz';
import RiddleQuiz from './components/pages/Quizzes/ImageMatch/ImageMatchQuiz';
import ForParents from './components/pages/ForParents';
import ForKids from './components/pages/ForKids';
import Hangman from './components/pages/Games/Hangman/Hangman';
import MatchMemory from './components/pages/Games/MatchMemory/MatchMemory';
import Jigsaw from './components/pages/Games/Jigsaw/Jigsaw';
import UserProfile from './components/pages/UserProfile';
import ProtectedRoute from './components/ProtectedRoute';

const handleLogout=()=>{
  localStorage.clear();
  window.location.pathname = "/signup"
}

function App() {
  return (
    <>
    <Router>
      <Navbar />
      <Routes>
        <Route path ='/' element={<Home/>} />
        <Route path ='/meals' element={<Meals/>} />
        <Route path ='/breakfast' element={<ProtectedRoute><Breakfast/></ProtectedRoute>} />
        <Route path ='/overnightoats' element={<OvernightOats/>} />
        <Route path ='/pancakes' element={<Pancakes/>} />
        <Route path ='/breakfastburrito' element={<BreakfastBurrito/>} />       
        <Route path ='/lunch' element={<Lunch/>} />
        <Route path ='/club-sandwich' element={<ClubSandwich/>} />
        <Route path ='/roll-ups' element={<RollUps/>} />
        <Route path ='/veg-soup' element={<VegSoup/>} />
        <Route path ='/dinner' element={<Dinner/>} />
        <Route path ='/chicken-veg-bowl' element={<ChickenVegBowl/>} />
        <Route path ='/pasta-and-sauce' element={<PastaSauce/>} />
        <Route path ='/roast-dinner' element={<RoastDinner/>} />
        <Route path ='/dessert' element={<Dessert/>} />
        <Route path ='/frozen-yoghurt' element={<FroYo/>} />
        <Route path ='/lollies' element={<Lollies/>} />
        <Route path ='/pavlova' element={<Pavlova/>} />
        <Route path ='/drinks' element={<Drinks/>} />
        <Route path ='/banana-smoothie' element={<BananaSmoothie/>} />
        <Route path ='/vitamin-booster' element={<VitaminBooster/>} />
        <Route path ='/avocado-straw-smoothie' element={<AvocadoStrawberry/>} />
        <Route path ='/snacks' element={<Snacks/>} />
        <Route path ='/lunchbox-snacks' element={<LunchboxSnacks/>} />
        <Route path ='/flapjacks' element={<Flapjacks/>} />
        <Route path ='/fruit-skewers' element={<FruitSkewers/>} />
        <Route path ='/signup' element={<Form/>} />
        <Route path ='/login' element={<Form2/>} />
        <Route path='/userprofile' element={<UserProfile/>} />
        <Route path ='/activities' element={<Activities/>} />
        <Route path ='/colouringsheets' element={<ColouringSheets/>} />
        <Route path ='/vegcolour' element={<VegColour/>} />
        <Route path ='/crosswords' element={<CrossWords/>} />
        <Route path ='/vegetablecrossword' element={<VegCrossword/>} />
        <Route path ='/wordsearches' element={<Wordsearches/>} />
        <Route path ='/vegword' element={<VegWord/>} />
        <Route path ='/oddoneoutquiz' element={<OddOneOutQuiz/>} />
        <Route path ='/riddlequiz' element={<RiddleQuiz/>} />
        <Route path ='/gardening' element={<Gardening/>} />
        <Route path ='/health' element={<HealthBenefits/>} />
        <Route path ='/tipstricks' element={<ParentingTips/>} />
        <Route path ='/forparents' element={<ForParents/>}/>
        <Route path ='/forkids' element={<ForKids/>}/>
        <Route path ='/hangman' element={<Hangman/>}/>
        <Route path='/matchmemory' element={<MatchMemory/>}/>
        <Route path='/jigsaw' element={<Jigsaw/>}/>

      </Routes>
      <Footer />
<button onClick={handleLogout}>Logout</button>
    </Router>
    </>
  );
}

export default App;
