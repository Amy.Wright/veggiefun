describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/jigsaw')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/forKids')
      })
    })

    describe('Jigsaw', () => {
      beforeEach(() => {
          cy.visit('/jigsaw')
        })
        it('instructions visible', () => {
          cy.get('body')
            .first()
          .find('.gameContainer')
          .find('.jigsaw')
          .find('h1')
          .should('be.visible')
        })
        it('fun facts visible', () => {
          cy.get('body')
            .first()
          .find('.gameContainer')
          .find('.jigsaw')
          .find('h2')
          .should('be.visible')
        })
        it('board visible', () => {
          cy.get('body')
            .first()
          .find('.gameContainer')
          .find('.jigsaw')
          .find('.board')
          .should('be.visible')
        })
        it('button visible', () => {
          cy.get('body')
            .first()
          .find('.gameContainer')
          .find('.jigsaw')
          .find('button')
          .should('be.visible')
        })
      })