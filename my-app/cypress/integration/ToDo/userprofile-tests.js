describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/userprofile')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/')
      })
    })

    describe('User Profile', () => {
      beforeEach(() => {
          cy.visit('/userprofile')
        })
        it('avatar is visible', () => {
          cy.get('body')
          .find('.userarea')
          .find('.left')
          .find('.Avatar')
          .should('be.visible')
        })
        it('change avatar is visible', () => {
          cy.get('body')
          .find('.userarea')
          .find('.left')
          .find('.changeAv')
          .should('be.visible')
        })
        it('user details visible', () => {
          cy.get('body')
          .find('.userarea')
          .find('.center')
          .find('.details')
          .should('be.visible')
        })
        it('likes visible', () => {
          cy.get('body')
          .find('.userarea')
          .find('.right')
          .find('.likes')
          .should('be.visible')
        })
        it('certificates visible', () => {
          cy.get('body')
          .find('.userarea')
          .find('.cert')
          .should('be.visible')
        })
      })