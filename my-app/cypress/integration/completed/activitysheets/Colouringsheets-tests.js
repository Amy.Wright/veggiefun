describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/vegcolour')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/colouringsheets')
      })
      
    })

    describe('Sheet', () => {
        beforeEach(() => {
            cy.visit('/vegcolour')
          })
          it('text visible', () => {
            cy.get('body')
              .first()
            .find('.pdfpage')
            .find('p')
            .should('be.visible')
          })
          it('sheet visible', () => {
            cy.get('body')
              .first()
            .find('.pdfpage')
            .find('.activity-pdf')
            .should('be.visible')
          })
          it('print button visible', () => {
            cy.get('body')
              .first()
            .find('.pdfpage')
            .find('.print-btns')
            .find('.btn-mobile')
            .should('be.visible')
          })
         
    })