describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
     
    })

    describe('Cards', () => {
        beforeEach(() => {
            cy.visit('/')
          })
          it('cards section visible', () => {
            cy.get('body')
              .first()
            .find('.cards')
            .should('be.visible')
          })
          it('header is visible', () => {
            cy.get('body')
              .first()
            .find('.cards')
            .get('h1')
            .should('be.visible')
          })
          it('cards are visible', () => {
            cy.get('body')
              .first()
            .find('.cards')
            .find('.cards-container')
            .find('.cards-wrap')
            .find('.cards-items')
            .children()
            .should('contain', 'For Parents')
            .should('contain', 'For Kids')
          })
          it('for parents link', () => {
            cy.get('body')
              .first()
            .find('.cards')
            .find('.cards-container')
            .find('.cards-wrap')
            .find('.cards-items')
            .find('.cards-items-link')
            .first()
            .click()
            .url().should('eq', 'http://localhost:3000/forparents')
          })
          it('for kids link', () => {
            cy.get('body')
              .first()
            .find('.cards')
            .find('.cards-container')
            .find('.cards-wrap')
            .find('.cards-items')
            .find('li')
            .next()
            .click()
            .url().should('eq', 'http://localhost:3000/forkids')
          })
        })
    