describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/gardening')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/forparents')
      })
      
    })

    describe('Sheet', () => {
        beforeEach(() => {
            cy.visit('/gardening')
          })
          it('text visible', () => {
            cy.get('body')
              .first()
              .find('div')
            .find('.inform-body')
            .should('be.visible')
          })
         
         
    })