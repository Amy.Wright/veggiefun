describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/health')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/forparents')
      })
      
    })

    describe('Sheet', () => {
        beforeEach(() => {
            cy.visit('/health')
          })
          it('text visible', () => {
            cy.get('body')
              .first()
              .find('div')
            .find('.inform-body')
            .should('be.visible')
          })
         
         
    })