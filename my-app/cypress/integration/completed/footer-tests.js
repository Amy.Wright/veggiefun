describe('Footer Visibility', () => {
    beforeEach(() => {
        cy.visit('/')
      })
      it('footer is visible', () => {
        cy.get('body')
          .first()
        .find('.footer-container')
        .should('be.visible')
      })
      it('subscription is visible', () => {
        cy.get('body')
          .first()
        .find('.footer-container')
        .find('.footer-subscription')
        .should('be.visible')
      })
    })

describe('Footer Links', () => {
    beforeEach(() => {
            cy.visit('/')
          })
          it('social media', () => {
            cy.get('body')
              .first()
            .find('.footer-container')
            .find('.footer-links')
            .find('.footer-link-wrapper')
            .find('.footer-link-items')
            .should('be.visible')
          })
          
        })

        describe('Footer Subscription', () => {
            beforeEach(() => {
                    cy.visit('/')
                  })
                  it('no input', () => {
                    cy.get('body')
                      .first()
                      .find('.footer-container')
                      .find('.footer-subscription')
                      .find('.footer-input')
                      .find('form')
                      .find('.btn-mobile')
                  })
                  it('input', () => {
                    cy.get('body')
                      .first()
                      .find('.footer-container')
                      .find('.footer-subscription')
                      .find('.footer-input')
                      .find('form')
                      .find('div')
                      .find('input')
                      .type('email@email.com')
                  })
                })