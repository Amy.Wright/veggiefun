describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/riddlequiz')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/forkids')
      })
      
    })

    describe('Quiz', () => {
        beforeEach(() => {
            cy.visit('/riddlequiz')
          })
          it('question is visible', () => {
            cy.get('body')
              .first()
            .find('.quizbody')
            .find('.q-title')
            .should('be.visible')
          })
          it('question number is visible', () => {
            cy.get('body')
              .first()
            .find('.quizbody')
            .find('.q-no')
            .should('be.visible')
          })
          it('options are visible', () => {
            cy.get('body')
              .first()
            .find('.quizbody')
            .find('.options')
            .should('contain', 'Strawberry')
            .should('contain', 'Orange')
            .should('contain', 'Pear')
            .should('contain', 'Apple')
          })
          it('next button is visible', () => {
            cy.get('body')
              .first()
            .find('.quizbody')
            .find('.next-button')
            .should('be.visible')
          })
        })
