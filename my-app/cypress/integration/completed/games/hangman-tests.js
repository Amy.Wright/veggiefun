describe('Banner', () => {
    beforeEach(() => {
        cy.visit('/hangman')
      })
      it('banner is visible', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .should('be.visible')
      })
      it('button works', () => {
        cy.get('body')
          .first()
        .find('.tab-container')
        .find('.tab-btns')
        .find('.btn-mobile')
        .click()
        .url().should('eq', 'http://localhost:3000/forKids')
      })
      
    })
    describe('Hangman', () => {
        beforeEach(() => {
            cy.visit('/hangman')
          })
          it('instructions are visible', () => {
            cy.get('body')
              .first()
            .find('.gameContainer')
            .find('.instructions')
            .should('be.visible')
          })
          it('frame is visible', () => {
            cy.get('body')
              .first()
              .find('.gameContainer')
              .find('.hangman-container')
              .find('.figure-container')
              .should('be.visible')
          })
          it('incorrect guesses are visible', () => {
            cy.get('body')
              .first()
              .find('.gameContainer')
              .find('.hangman-container')
              .find('div')
              .should('be.visible')
          })
          it('word is visible', () => {
            cy.get('body')
              .first()
              .find('.gameContainer')
              .find('.hangman-container')
              .find('.word')
              .should('be.visible')
          })
        })