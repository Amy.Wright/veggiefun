const express = require('express')
const app = express()
const mysql = require('mysql2')
const cors = require('cors')

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
    user: 'root',
    host:'localhost',
    password: 'veggiefun',
    database: 'veggiefunusers'
});

app.post('/create', (req, res) => {
    const name = req.body.name
    const email = req.body.email
    const username = req.body.username
    const password = req.body.password

    db.query('INSERT INTO users (name, email, username, password) VALUES (?,?,?,?)', [name, email, username, password], (err, result)=> {
        if(err) {
            console.log(err)
        } else {
            res.send('values inserted')
        }
    })
})

app.post('/login', (req,res) => {
    const username = req.body.username
    const password = req.body.password

    db.query('SELECT * FROM users WHERE username = ? AND password = ?',[username, password], (err, result)=> {
            if (err){
            res.send({err: err})
        }
            if (result.length > 0) {
                res.send(result)
            }else {
                res.send({message: "Incorrect username or password"})
            }
    })
})

app.post('/newsletter', (req, res) => {
    const email = req.body.email

    db.query('INSERT INTO newsletter (email) VALUES (?)', [email], (err, result)=> {
        if(err) {
            console.log(err)
        } else {
            res.send('values inserted')
        }
    })
})

app.listen(3001, () => {
    console.log("running on 3001");
})